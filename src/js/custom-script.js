var preloader = document.getElementById('preloader-js');
var onceExecFunc = true;
var modalCreateRacion = document.querySelector('.modal--create-racion');
var activeElement = $('.categories__item--active');
var indexActiveElement = activeElement.index();
var groupCreated = false;
var containerCurrentNumGalleryBlock = $('.gallery-block__current-num');
var containerTotalNumGalleryBlock = $('.gallery-block__total-num');
var activeFoodName = $('.order-by-menu__carousel-item.slick-active').find('.order-by-menu__carousel-image-wrapper').attr('data-food-name');
var containerFoodName = $('.order-by-menu__food-name');
var containerCurrentNum = $('.order-by-menu__current-num');
var containerTotalNum = $('.order-by-menu__total-num');
var mediaDownLG = window.matchMedia("screen and (max-width: 991px)").matches;
var mediaUpLG = window.matchMedia("screen and (min-width: 992px)").matches;
//new27
var mediaDownLGRacion = window.matchMedia("screen and (max-width: 1024px)").matches;
var mediaUpLGRacion = window.matchMedia("screen and (min-width: 1025px)").matches;

//READY-PROGRAMS BG PARALLAX
var scene = $('.ready-programs__bg-scene').get(0);

if (scene) {
	var parallax = new Parallax(scene);
}

if (modalCreateRacion) {
	var normKkalValue = modalCreateRacion.dataset.normValue;
	var calculateNormKkalValue = normKkalValue;
	var modalCreateRacionValue = document.querySelector('.modal__personal-value');
	modalCreateRacionValue.append(normKkalValue);
	var dataJson = document.querySelector('.menu-personal-block__list[data-json]');

	if (dataJson) {
		var urlJson = dataJson.dataset.json;
	}

	var modalMenuPersonal = document.querySelector('.modal--menu-personal');
	var modalMenuPersonalTitle = modalMenuPersonal.querySelector('.modal__title--menu-personal');
	modalMenuPersonalTitle.append(' ' + normKkalValue + ' kkal');

	var modalDayKkalValue = modalMenuPersonal.querySelector('.modal__days-kkal-value');
	modalDayKkalValue.textContent = normKkalValue;
}

var selectedItem = new Object();
var racionSwiperCheck = document.querySelector('.racion .swiper-container');

if (racionSwiperCheck) {
	var racionSwiper = new Swiper('.racion .swiper-container', {
		observer: true,
		effect: 'fade',
		prevButton: '.racion .swiper-button-prev',
		nextButton: '.racion .swiper-button-next',
		simulateTouch: false,
		fade: {
			crossFade: true
		},
		breakpoints: {
			1024: {
				simulateTouch: true,
				effect: 'slide',
				slidesPerView: 'auto',
				freeMode: true
			}
		},
		onSlideChangeEnd: function() {

			if (window.matchMedia("screen and (min-width: 1200px)").matches) {
				disableDroppableAreas();
				enableDroppableAreas();
			}
		}
	});
}

var checkActiveDishesSection = false;

var modalCreateRacionBg = $('.modal--menu-personal .modal__bg');
var modalCreateRacionBgHeight = modalCreateRacionBg.outerHeight();

// Source: https://github.com/jserz/js_piece/blob/master/DOM/ParentNode/append()/append().md
(function (arr) {
	arr.forEach(function (item) {
		if (item.hasOwnProperty('append')) {
			return;
		}
		Object.defineProperty(item, 'append', {
			configurable: true,
			enumerable: true,
			writable: true,
			value: function append() {
				var argArr = Array.prototype.slice.call(arguments),
					docFrag = document.createDocumentFragment();

				argArr.forEach(function (argItem) {
					var isNode = argItem instanceof Node;
					docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
				});

				this.appendChild(docFrag);
			}
		});
	});
})([Element.prototype, Document.prototype, DocumentFragment.prototype]);



$(function() {
		//JQuery UI Datapicker
	$('.field-text__input--date:not(.field-text__inpu--racion)').datepicker({
		minDate: 0,
		beforeShow: function() {
			var placeholder = $(this).next().next();

			if (!placeholder.hasClass('field-text__name--hide')) {
				placeholder.addClass('field-text__name--hide');
			}

			setTimeout(function(){
					$('.ui-datepicker').css('z-index', 5);
			}, 0);
		},
		onClose: function(date) {
			var placeholder = $(this).next().next();
			if (!date) {

				if (placeholder.hasClass('field-text__name--hide')) {
					placeholder.removeClass('field-text__name--hide');
				}
			} else {
				$(this).val(date + '');
			}
		},
		onSelect: function(dateText, inst) {
			var placeholder = $(this).next().next();

			if (!placeholder.hasClass('field-text__name--hide')) {
				placeholder.addClass('field-text__name--hide');
			}
		},
		showOn: "button",
		buttonImage: "/local/templates/.default/libs/img/calendar-icon.png",
		buttonImageOnly: true,
		buttonText: "Выберите дату"
	});

	$('.field-text__inpu--racion').datepicker({
		minDate: 2,
		beforeShow: function() {
			var placeholder = $(this).next().next();

			if (!placeholder.hasClass('field-text__name--hide')) {
				placeholder.addClass('field-text__name--hide');
			}

			$(this).next('.field-text__name').addClass('field-text__name--hide');
			setTimeout(function(){
					$('.ui-datepicker').css('z-index', 105);
			}, 0);
		},
		onSelect: function(dateText, inst) {
			var placeholder = $(this).next().next();

			if (!placeholder.hasClass('field-text__name--hide')) {
				placeholder.addClass('field-text__name--hide');
			}
		},
		onClose: function(date) {
			var placeholder = $(this).next().next();

			if (!date) {

				if (placeholder.hasClass('field-text__name--hide')) {
					placeholder.removeClass('field-text__name--hide');
				}
			} else {
				$(this).val(date + '');
			}
		},
		showOn: "button",
		buttonImage: "/local/templates/.default/libs/img/calendar-icon.png",
		buttonImageOnly: true,
		buttonText: "Выберите дату"
	});

	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
		'Июл','Авг','Сен','Окт','Ноя','Дек'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false
	};
	$.datepicker.setDefaults($.datepicker.regional['ru']);

	$.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){
		if (isFixed) {
			var heightFromTop = $('.modal__field-item .field-text__input--date').offset();
			var heightFromTopDateReg = $('.field-text__input--day').offset();
			if (heightFromTop.top > 0) {
				offset.top = heightFromTop.top - $(document).scrollTop() - 50;
				offset.left = heightFromTop.left - $(document).scrollLeft();
				return offset;
			} else if (heightFromTopDateReg.top > 0) {
				offset.top = heightFromTopDateReg.top - $(document).scrollTop() - 50;
				offset.left = heightFromTopDateReg.left - $(document).scrollLeft();
				return offset;
			}
		} else {
			return offset;
		}
	}});

	$('.field-text__input--day').datepicker({
		changeYear:true,
		changeMonth: true,
		yearRange: "-100:+0",
		beforeShow: function() {
			setTimeout(function(){
					$('.ui-datepicker').css('z-index', 105);
			}, 0);
		},
		onSelect: function(dateText, inst) {
			var date = $(this).datepicker('getDate'),
					day  = date.getDate(),
					month = date.getMonth() + 1,
					year =  date.getFullYear();
			$('.field-text__input--day').val(day);
			$(this).next('.field-text__name').addClass('field-text__name--hide');
			$('.field-text__input--month').val(month);
			$('.field-text__input--month').next('.field-text__name').addClass('field-text__name--hide');
			$('.field-text__input--year').val(year);
			$('.field-text__input--year').next('.field-text__name').addClass('field-text__name--hide');
		}
	});



	$('.field-text__date-reg-btn').click(function(event) {
		$('.field-text__input--day').datepicker('show');
	});

})

jQuery(document).ready(function($) {
	clickAddDishesBtnPopup();
	checkBtnRemoveDay();
	checkBtnDoubleDay();
	checkBtnsDoubleWeek();
	clickRemoveDayPopup();
	showCreateRacionModal(600);
	hideCreateRacionModal(600);

	setDataToggle();
	changeIntakes();

	$('.modal__weeks').accordion({
		active: false,
		collapsible: true,
		header: '> .modal__weeks-item-header',
		heightStyle: 'content',
		icons: false,
		activate: function(event, ui) {
			//new27
			if (mediaUpLGRacion) {
				var modalBlock = $('.modal--menu-personal .modal__block');
				var modalHeight = modalBlock.outerHeight();
				var res = modalCreateRacionBgHeight + 17 - modalHeight;

				if (res < 0) {
					modalCreateRacionBg.css('bottom', res + 'px');
				} else if (res >= 0) {
					modalCreateRacionBg.css('bottom', 0 + 'px');
				}
			}
		}
	});

	$('.modal__days').accordion({
		active: false,
		collapsible: true,
		header: '> .modal__days-item-header',
		heightStyle: 'content',
		icons: false,
		activate: function(event, ui) {

			if (mediaUpLGRacion) {
				var modalBlock = $('.modal--menu-personal .modal__block');
				var modalHeight = modalBlock.outerHeight();
				var res = modalCreateRacionBgHeight + 17 - modalHeight;

				if (res < 0) {
					modalCreateRacionBg.css('bottom', res + 'px');
				} else if (res >= 0) {
					modalCreateRacionBg.css('bottom', 0 + 'px');
				}
			}
		}
	});

	selectItemRacionModalDescr();

	addToRacionFromModal();

	showEatTimeModal();

	createRacion();

	dragMenuPersonal();

	checkMenuPersonalPage();

	getLastNavItem();

	dayTimeDisclosure();

	checkInputs();


	//Starting settings


	/*Switching power supply start*/
		$('[name=switchin_number_meals]').change(function(event) {

			var number = $(this).data('number');

			if (number === "five") {
				$('.schedule__daybox-js').addClass('schedule__daybox--five');
			}
			else {
				$('.schedule__daybox-js').removeClass('schedule__daybox--five');
			}

		});
	/*Switching power supply start*/


	/*Accordion in expert order start*/
		$('.schedule__titleweek-js').click(function(event) {
			$(this).toggleClass('schedule__title--close');
			$(this).next('.schedule__weekbox-js').slideToggle(400)
			console.log($(this))
		});

		$('.schedule__titleday-js').click(function(event) {
			$(this).toggleClass('schedule__title--close');
			$(this).next('.schedule__daybox-js').slideToggle(400)
			console.log($(this))
		});
	/*Accordion in expert order end*/

	/*Adding new days to the order start*/
	var indexDayClone  = 2;
	var indexWeekClone = 1;
	$('.add-day-js').click(function(event) {

		if (indexDayClone === 1) {
			$('.schedule__week:last .schedule__weekbox .schedule__addday').remove();

			var week = $('.schedule__weekclone').clone(true, true);
			week
				.find('.schedule__numberweek')
				.html(indexWeekClone);
			week
				.removeClass('schedule__weekclone')
				.appendTo('.schedule__weeklist')
				.show();

		}


		var shedule = $('.schedule__clone').clone(true, true);

		shedule.insertBefore('.schedule__week:last .schedule__weekbox .schedule__addday');
		shedule
			.find('.schedule__number')
			.html(indexDayClone);
		shedule.removeClass('schedule__clone')
		shedule.show();

		indexDayClone++;

		if (indexDayClone === 8) {
			indexDayClone = 1;
			indexWeekClone ++;
		}

		return false;
	});
	/*Adding new days to the order end*/


	$('.dish__close-js').click(function(event) {
		$(this).parents('.schedule__dish--open').removeClass('schedule__dish--open');
		$(this).parents('.dish').remove();
	});

 $('.dish__mobile-js').click(function(event) {
	$(this).hide();
	$('.dish__navlist-js').show();
	return false;
 });

 $('.dish__navlist-js a').click(function(event) {
	// Для мобильных
	if (window.matchMedia("screen and (max-width: 767px)").matches) {
		var text = $(this).html();
		$('.dish__navlist-js').hide();
		$('.dish__mobile-js').show()
		.html(text);
	}
	return false;
 });

	$('[data-toggle-activate]').click(function(event) {
		$($(this).attr('data-toggle-activate')).toggleClass('active');
	});

	$('[data-toggle-activate-multiple]').click(function(event) {
		var list = JSON.parse($(this).attr('data-toggle-activate-multiple'));
		$(list).each(function(index, el) {
			$(el).toggleClass('active');
		});
	});

	$('[data-toggle-show]').click(function(event) {
		var targetBlock = $($(this).attr('data-toggle-show'));
		if  (targetBlock.css('display')=="none"){
			targetBlock.fadeIn(400);
		}else{
			targetBlock.fadeOut(400);
		}
	});

	$(document).on('focus', '.field-text__input', function(event) {
		$(this).next('.field-text__name').addClass('field-text__name--hide');

		if ($(this).hasClass('field-text__input--date')) {
			var datePlaceholder = $(this).next().next();
			datePlaceholder.addClass('field-text__name--hide');
		}
	});

	$(document).on('focusout', '.field-text__input:not(.field-text__input--date)', function(event) {

		if ($(this).val().trim() === '') {

			if ($(this).next('.field-text__name').hasClass('field-text__name--hide')) {
				$(this).next('.field-text__name').removeClass('field-text__name--hide');
			}
		}
	});

	$(document).on('focusout', '.field-text__input--date', function(event) {
		var input = $(this);
		var datePlaceholder = $(this).next().next();

		setTimeout(function() {
			var date = input.val();

			if (!date) {
				datePlaceholder.removeClass('field-text__name--hide');
			}
		}, 200);
	});

	$(document).on('click', '[data-fadeIn]', function(event) {
		event.preventDefault();
		noScrollBody($(this).attr('data-fadeIn'), $(this).attr('data-scroll'));
		calculateModalIndents($(this).attr('data-fadeIn'));
		$($(this).attr('data-fadeIn')).fadeIn(400);
	});

	$(document).on('click', '[data-fadeOut]', function(event) {
		event.preventDefault();
		noScrollBody($(this).attr('data-fadeOut'), $(this).attr('data-scroll'));
		$($(this).attr('data-fadeOut')).fadeOut(400);
	});


	$('[data-fadeout=".modal--expert-racion"]').click(function(event) {
		setTimeout(function() {
			$('body').removeClass('no-scroll');

		}, 200);
	});

	//Show/hide page header search section
	$('.page-header__search-btn').click(function(event) {
		if (!($('.page-header__top-content').hasClass('page-header__top-content--hide'))) {
			$('.page-header__top-content').addClass('page-header__top-content--hide');
		}
	});

	$('.page-header__search-close').click(function(event) {
		if ($('.page-header__top-content').hasClass('page-header__top-content--hide')) {
			$('.page-header__top-content').removeClass('page-header__top-content--hide');
		}
	});

	//Top slider
	$('.top-slider__container').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		infinite: true,
		speed: 900,
		fade: true,
		cssEase: 'linear'
	});

	//FEATURES-CHOOSE__SLIDER
	$('.features-choose__slider').slick(getFeaturesChooseSliderSettings());

	//Gallery block slider
	$('.gallery-block__slider').slick({
		infinite: true,
		initialSlide: 1,
		centerMode: true,
		variableWidth: true
	});

	containerCurrentNumGalleryBlock.text($('.gallery-block__slider .slick-active').index() - 1);
	containerTotalNumGalleryBlock.text($('.gallery-block__item').length - 4);

	$('.gallery-block__slider').on('afterChange', function(event, slick, currentSlide) {
		containerCurrentNumGalleryBlock.text(currentSlide + 1);
	});

	//dotdotdot Plugin

	//Set indent for slick-dots on load page
	leftPositionDotsTopSlider();

	//Set margin-left for slide-content on load page
	marginLeftSlideContentTopSlider();

	//Set top position for arrows on load page
	topPositionArrowsTopSlider();

	//Set right position for arrows on load page
	rightPositionArrowsTopSlider();

	//Add padding-left for enroll-nutritionist__left
	addWidthNutrLeft();

	//Add padding-left and margin right when width > 1200
	healthCenterBox();

	marginFeaturesChooseLeft()

	paddingFeaturesChooseRight();

	instagramHide();

	checkRadio();

	repositionCategoriesItems();

	generateSlideGroups();

	if (!$('.page-header--expert').length > 0) {
		navScrollMonitor();
	}

	addMarginIndividualRacionLeft();

	checkBottomDelimiter();

	checkTopDelimiter();

	addMarginExpertFeatures();

	showCategories();

	togglePanelsItem();

	dotPlugin();

	addMarginLeftCalculatorBlock($('.calculator-block__content'), $(window).width());

	setSelectWidth($('.field-text--select-calc'), $(window).width());


	if (!$('.page-header--expert').length > 0) {
		scrollFromTop();
	}




	$('.sorting__list-js .sorting__item').click(function(event) {
		$(this).toggleClass('sorting__item--incr');
		$(this).toggleClass('sorting__item--reduce');
		return false;
	});

	//REQUIRED INPUTS
	$('.field-text__input[required]').each(function(index, el) {
		var requiredPlaceholderText = $(el).siblings('.field-text__name').text();
		$(el).siblings('.field-text__name').html(requiredPlaceholderText + '<span class="field-text__green-star"> *</span>');
	});

	//READY-PROGRAMS
	$('.ready-programs__item').each(function(index, el) {

		//ITEM GALLERY
		$(el).find('.ready-programs__item-gallery').slick({
			arrows: true,
			speed: 900,
			fade: true,
			cssEase: 'linear'
		});
	});

	//ORDER-BY-MENU SLIDER
	$('.order-by-menu__carousel').slick({
		arrows: true,
		speed: 900,
		fade: true,
		infinite: false,
		cssEase: 'linear'
	});

	containerFoodName.text(activeFoodName);
	containerCurrentNum.text($('.order-by-menu__carousel-item.slick-active').index() + 1);
	containerTotalNum.text($('.order-by-menu__carousel-item').length);
	$('.order-by-menu__food-name').addClass('order-by-menu__food-name--active');

	$('.order-by-menu__carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide) {

		setTimeout(function() {
			var carousel = $('.order-by-menu__carousel');
			var dataAttr = carousel.find('.order-by-menu__carousel-item').eq(nextSlide).find('.order-by-menu__carousel-image-wrapper').attr('data-food-name');
			containerFoodName.text(dataAttr);
			$('.order-by-menu__food-name').addClass('order-by-menu__food-name--active');
		}, 1000);

		$('.order-by-menu__food-name').removeClass('order-by-menu__food-name--active');
	});

	$('.order-by-menu__carousel').on('afterChange', function(event, slick, currentSlide) {
		containerCurrentNum.text(currentSlide + 1);
	});

	checkDataPicker();

	//Reviews carousel

	$('.reviews__carousel').slick({
		infinite: true,
		mobileFirst: true,
		responsive: [{

			breakpoint: 767,
			settings: {
				slidesToShow: 2
			}

		}, {

			breakpoint: 991,
			settings: {
				slidesToShow: 3
			}

		}]
	})

	//Jquery form styler
	$('select').styler();

	if (document.querySelector('.breadcrumbs')) {
		calculateHeight('set', document.querySelector('.page-header'), document.querySelector('.breadcrumbs').offsetHeight, 170);
	}

	//Order history
	$('.orders-history-block__table-body').accordion({
		active: false,
		collapsible: true,
		header: '> .orders-history-block__table-item-header',
		heightStyle: 'content',
		icons: false
	});
});

$(window).scroll(function(event) {
	removeFixedNavContainer();

	fixedRacion('add');
	fixedRacion('remove');
});


	if (!$('.page-header--expert').length > 0) {
		$(window).scroll(function(event) {
			scrollFromTop();
		});
	}



$(window).load(function() {

	topPositionArrowsTopSlider();
	BlockMaxHeight('.menu-personal-block__item .menu-personal-block__item-title');

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		BlockMaxHeight('.menu-categories-block__item');
		BlockMaxHeight('.intake__item');

		BlockMaxHeight('.menu-personal-block__item');

		setAbsolute('.menu-categories-block__item-wrapper');
		setAbsolute('.intake__item-wrapper');
		setAbsolute('.menu-personal-block__item-wrapper');
	}

	if (preloader) {

		preloader.classList.add('preloader--hide');

		setTimeout(function() {
			preloader.classList.add('preloader--display-none');

			var body = document.querySelector('body');

			if (body.classList.contains('no-scroll-preloader')) {
				body.classList.remove('no-scroll-preloader');
			}
		}, 1500);
	}

	try {
		$.browserSelector();
		if($("html").hasClass("page")) {
			$.smoothScroll();
		}
	} catch(err) {};

	if (mediaUpLGRacion) {
		$('#racion-js').mCustomScrollbar({
			scrollButtons: {
				enable: true
			},
			theme: 'medline'
		});
	}

	btnsDoubleDay();

	setSelectWidth($('.field-text--select-calc'), $(window).width());
});

//BASE FUNC
$(window).resize(function(event) {

	//new27
	setHeightRacionItemTitle();

	$('.top-slider__container').slick('setPosition');

	//Set indent for slick-dots on resize
	leftPositionDotsTopSlider();

	//Set margin-left for slide-content on resize
	marginLeftSlideContentTopSlider();

	//Set top position for arrows on resize
	topPositionArrowsTopSlider();

	//Set right position for arrows on resize
	rightPositionArrowsTopSlider();

	//Add width for enroll-nutritionist__left on resize
	addWidthNutrLeft();

	//Add padding-left and margin right when width > 1200 on resize
	healthCenterBox();

	generateSlideGroups();

	marginFeaturesChooseLeft();

	paddingFeaturesChooseRight();

	instagramHide();

	// heightParallax();

	repositionCategoriesItems();
	if (!$('.page-header--expert').length > 0) {
		navScrollMonitor();
	}

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {

		if (!onceExecFunc) {
			BlockMaxHeight('.menu-categories-block__item');
			setAbsolute('.menu-categories-block__item-wrapper');


			BlockMaxHeight('.intake__item');
			setAbsolute('.intake__item-wrapper');


			BlockMaxHeight('.menu-personal-block__item');
			setAbsolute('.menu-personal-block__item-wrapper');

			onceExecFunc = true;
		}
	} else {
		$('.menu-categories-block__item').attr('style', '');
		$('.menu-categories-block__item-wrapper').removeClass('menu-categories-block__item-wrapper--absolute');

		$('.intake__item').attr('style', '');
		$('.intake__item-wrapper').removeClass('menu-categories-block__item-wrapper--absolute');

		$('.menu-personal-block__item').attr('style', '');
		$('.menu-personal-block__item-wrapper').removeClass('menu-categories-block__item-wrapper--absolute');

		onceExecFunc = false;
	}

	addMarginIndividualRacionLeft();

	addMarginExpertFeatures();

	showCategories();

	togglePanelsItem();

	setSelectWidth($('.field-text--select-calc'), $(window).width());

	addMarginLeftCalculatorBlock($('.calculator-block__content'), $(window).width());

	setDataToggle();

	recalculateRacionSwiper();

	dayTimeDisclosure();

});


//new27
function setHeightRacionItemTitle() {
	let titles = document.querySelectorAll('.menu-personal-block__item-title');

	for (var i = 0; i < titles.length; i++) {
		titles[i].style.height = 'auto';
	}

	BlockMaxHeight('.menu-personal-block__item-title');
}

function getFeaturesChooseSliderSettings() {
	return {
		mobileFirst: true,
		infinite: false,
		responsive: [{

				breakpoint: 767,
				settings: {
					slidesToShow: 2
				}

			},{

				breakpoint: 1024,
				settings: {
					slidesToShow: 1
				}
		}]
	}
}

function checkInputs() {
	var inputs = document.querySelectorAll('.field-text__input:not(.field-text__input--number)');

	if (inputs.length > 0) {

		for (var i = 0; i < inputs.length; i++) {

			if (inputs[i].value.trim()) {

				if (inputs[i].nextElementSibling) {

					if (!(inputs[i].nextElementSibling.classList.contains('field-text__name--hide'))) {
						inputs[i].nextElementSibling.classList.add('field-text__name--hide');
					}
				}
			}
		}
	}
}

function dayTimeDisclosure() {

	var shortLink = document.querySelectorAll('.daytime__link');

	if (shortLink.length > 0) {

		for (var i = 0; i < shortLink.length; i++) {

			if (mediaDownLG) {
				var attr = shortLink[i].dataset.shortcut;
			} else if (mediaUpLG) {
				var attr = shortLink[i].dataset.fullname;
			}

			if (attr !== undefined && attr.length > 0) {
				shortLink[i].textContent = attr;
			}
		}
	}
}

function addMarginLeftCalculatorBlock(element, windowWidth) {
	var marginLeft = ((windowWidth - 1200) / 2) + 'px';

	if (window.matchMedia("screen and (min-width: 1201px)").matches) {
		element.css('margin-left', marginLeft);
	} else {
		element.css('margin-left', '0');
	}
}

function dotPlugin() {
	$('.news-block__item-descr').dotdotdot({
		ellipsis: '... ',
		height: 100,
		watch: true
	});

	$('.news__text-elepsis').dotdotdot({
		ellipsis: '... ',
		height: 100,
		watch: true
	});
}

function setSelectWidth(select, windowWidth) {
	var maxWidth = windowWidth - 30;
	var dropdown = select.find('.jq-selectbox__dropdown');

	if (window.matchMedia("screen and (min-width: 768px)").matches) {
		maxWidth =  ((maxWidth / 3) * 2);
		select.css('max-width', maxWidth);
		dropdown.css('width', 'auto');
		dropdown.css('min-width', '100%');
	} else if (window.matchMedia("screen and (min-width: 320px) and (max-width: 767px)").matches) {
		select.css('max-width', maxWidth + 'px');
		dropdown.css('width', maxWidth + 'px');
	} else {
		select.css('max-width', '290px');
		dropdown.css('width', '290px');
	}
}

function increaseLike(elem) {
	var numCurrentLike = +elem.text();

	elem.text(numCurrentLike + 1);
}

function togglePanelsItem() {

	$('.docs-block__panels-item-title').click(function() {
		var $currentElem = $(this);
		if ($currentElem.parents('.docs-block__panels-item').hasClass('docs-block__panels-item--active')) {
			setTimeout(function() {
				$currentElem.parents('.docs-block__panels-item').removeClass('docs-block__panels-item--active');
			}, 5);
		} else {
			$('.docs-block__panels-item').removeClass('docs-block__panels-item--active');
			setTimeout(function() {
				$currentElem.parents('.docs-block__panels-item').addClass('docs-block__panels-item--active');
			}, 10);
		}
	});
}

function showCategories() {

	//new27
	if (window.matchMedia("screen and (max-width: 767px)").matches) {
		$(document).on('click','.categories__item--active',function(event) {
			$('.categories__list').toggleClass('categories__list--open');
		});
	}
}

function addMarginExpertFeatures() {
	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		var marginLeft = ($(window).width() - 1200) / 2;
		$('.expert-block__features-content').css('margin-left', marginLeft + 'px');
	} else {
		$('.expert-block__features-content').attr('style', '');
	}
}

function checkTopDelimiter() {

	if ($('main').hasClass('no-top-delimiter')) {
		$('.page-header').addClass('page-header--no-delimiter');
	}
}

function checkBottomDelimiter() {

	if ($('main').hasClass('no-bottom-delimiter')) {
		$('.page-footer').addClass('page-footer--no-delimiter');
	}

	if ($('main').hasClass('bottom-delimiter')) {
		$('.page-footer').addClass('page-footer--delimiter-menu-personal');
	}
}

function addMarginIndividualRacionLeft() {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		var marginLeft = ($(window).width() - 1200) / 2;
		$('.individual-racion-block__left').css('margin-left', marginLeft + 'px');
	} else {
		$('.individual-racion-block__left').attr('style', '');
	}
}

function menuCategoriesSort() {
	var sortedLinks = $('.sorting__link');

	sortedLinks.click(function(event) {
		event.preventDefault();
		var parentSortedLink = $(this).parents('.sorting__item');

		if (parentSortedLink.hasClass('sorting__item--incr') || parentSortedLink.hasClass('sorting__item--reduce')) {
			parentSortedLink.toggleClass('sorting__item--incr  sorting__item--reduce');
		} else {
			$('.sorting__link').parents('.sorting__item').removeClass('sorting__item--incr  sorting__item--reduce');
			parentSortedLink.addClass('sorting__item--incr');
		}
	});
}

function navScrollMonitor() {
	//Pahe-header scroll monitor
	var navWatcher = scrollMonitor.create($('.page-header__nav-container'));

	navWatcher.exitViewport(function() {
		if (window.matchMedia("screen and (max-width: 767px)").matches) {
			$('.page-header__nav-container').addClass('page-header__nav-container--fixed');
		}
	});

	if (window.matchMedia("screen and (min-width: 768px)").matches) {
		$('.page-header__nav-container').removeClass('page-header__nav-container--fixed');
	}
}

function repositionCategoriesItems() {
	var categories     = $('.categories__item');
	var listCategories = $('.categories__list');

	if (indexActiveElement > 0) {

		if (window.matchMedia("screen and (max-width: 767px)").matches) {
			activeElement.remove();
			listCategories.prepend(activeElement);
		} else if (window.matchMedia("screen and (min-width: 768px)").matches) {
			var preElement = $('.categories__item').eq(indexActiveElement - 1);
			activeElement.remove();
			preElement.after(activeElement);
		}
	}
}

function heightParallax() {
	var heightHealthCenter = $('.health-center').outerHeight();
	$('.parallax-mirror').css('max-height', heightHealthCenter + 'px');
}

function checkRadio() {
	$('.field-radio__input[type="radio"]').each(function(index, el) {
		if ($(el).is(':checked')) {
			$(el).parents('.field-radio__name').addClass('field-radio__name--checked');
		}
	});

	$('.field-radio__input[type="radio"]').change(function() {
		$('.field-radio__input[type="radio"]').parents('.field-radio__name').removeClass('field-radio__name--checked');

		if ($(this).is(':checked')) {
			$(this).parents('.field-radio__name').addClass('field-radio__name--checked');
		}
	});
}

function scrollFromTop() {

	var headerElem = $('.page-header');
	var pageMain = $('.page__main');

	if (window.matchMedia("screen and (min-width: 768px)").matches) {

		if ($(window).scrollTop() > 170) {
			headerElem.addClass('page-header--shadow');
			pageMain.addClass('page__main--fixed');
		} else {
			headerElem.removeClass('page-header--shadow');
			pageMain.removeClass('page__main--fixed');
		}
	} else {
		headerElem.removeClass('page-header--shadow');
		pageMain.removeClass('page__main--fixed');
	}
}

function removeFixedNavContainer() {
	var navContainer = $('.page-header__nav-container');
	if (window.matchMedia("screen and (max-width: 767px)").matches) {
		if ($(window).scrollTop() < 170) {
			navContainer.removeClass('page-header__nav-container--fixed');
		}
	}
}

function topPositionArrowsTopSlider() {
	var imgSliderHeight = $('.top-slider__item-img').height(),
			heightFromTop   = 0;

	if (window.matchMedia("screen and (min-width: 768px)").matches) {

		heightFromTop =  imgSliderHeight - 100;

	} else {

		heightFromTop =  imgSliderHeight - 57;

	}

	$('.top-slider .slick-arrow').css('top', heightFromTop + 'px');
}

function BlockMaxHeight(selector) {
	var list = $(selector);
	list.attr('style', '');
	list.height(Math.max.apply(Math, list.map(function(index, elem) {return $(elem).height()})));
}

function setAbsolute(selector) {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		$(selector).addClass('menu-categories-block__item-wrapper--absolute');
	} else {
		$(selector).removeClass('menu-categories-block__item-wrapper--absolute');
	}
}

function calculateHeight(state, header, breadcrumbsHeight, scrollTop) {

	if (mediaUpLGRacion) {
		var racion = document.getElementById('racion-js');

		if (racion) {

			if (state = 'set') {
				var swiperContainer = document.querySelector('.racion .swiper-container');

				var viewHeight = document.documentElement.clientHeight;
				var headerHeight = header.offsetHeight;

				swiperContainer.style.paddingBottom = scrollTop + 'px';

				var heightRacion = viewHeight - headerHeight - breadcrumbsHeight + scrollTop + 1;// +1 - margin-top: -1(div.menu-personal-block);

				racion.style.height = heightRacion + 'px';
			} else if (state = 'reset') {
				racion.style.height = 'auto';
			}
		}
	}
}

function calculateMarginRightMenuPersonalMain(widthRacion) {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		var block = document.getElementById('menu-personal-block-js');
		block.style.marginRight = widthRacion + 'px';
	}
}

function fixedRacion(state) {

	if (mediaUpLGRacion) {
		var racion = document.getElementById('racion-js');
		var block = document.getElementById('menu-personal-block-js');

		if (racion) {
			var scroll = window.pageYOffset;
			var pageFooter = document.querySelector('.page__footer-wrapper');

			if (state === 'add') {

				if (scroll > 170) {

					if (!racion.classList.contains('racion--fixed')) {
						racion.classList.add('racion--fixed');
						calculateMarginRightMenuPersonalMain(racion.offsetWidth);
						calculateHeight('set', document.querySelector('.page-header'), 0, 170);
					}
				}
			} else if (state === 'remove') {

				if (scroll < 170) {

					if (racion.classList.contains('racion--fixed')) {
						racion.classList.remove('racion--fixed');
						calculateMarginRightMenuPersonalMain(0);
						calculateHeight('set', document.querySelector('.page-header'), document.querySelector('.breadcrumbs').offsetHeight, 170);
					}
				}
			}
		}
	}
}

// Insta script
function instagramHide() {
	var instaItems = $('.instagram__item');

	if (window.matchMedia("screen and (min-width: 768px) and (max-width: 991px)").matches) {
		instaItems.each(function(index, el) {
			if (index >= 4) {
				$(el).addClass('instagram__item--hide');
			}
		});
	} else {
		instaItems.removeClass('instagram__item--hide');
	}
}

function paddingFeaturesChooseRight() {
	var rightBlock = $('.features-choose__right');

	if (window.matchMedia("screen and (min-width: 768px) and (max-width: 991px)").matches) {
		rightBlock.css('width', 600 + (($(window).width() - 1200) / 2) + 'px');
	} else {
		rightBlock.attr('style', '');
	}
}

function marginFeaturesChooseLeft() {
	var leftBlock = $('.features-choose__left');

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		leftBlock.css('margin-left', (($(window).width() - 1200) / 2) + 'px');
	} else {
		leftBlock.attr('style', '');
	}
}

//new27
function generateSlideGroups() {
	var slides = $('.features-choose__slider-item');
	var arraySlides = slides.toArray();
	var slider = $('.features-choose__slider');
	var htmlGroup = '<div class="features-choose__slider-group"></div>';
	var numOfGroups = Math.ceil(slides.length / 4);
	var numOfItemsInGroup = 4;

	if (window.matchMedia("screen and (min-width: 1025px)").matches && groupCreated === false) {
		$('.features-choose__slider').slick('unslick');
		groupCreated = true;

		//Create groups
		for (var i = 0; i < numOfGroups; i++) {
			slider.prepend(htmlGroup);
		}

		var createdGroups = $('.features-choose__slider-group');

		// //Push slides inside groups
		createdGroups.each(function(index, el) {
			for (var i = 0; i < numOfItemsInGroup; i++) {
				$(el).append(arraySlides.shift());
			}
		});
		$('.features-choose__slider').slick(getFeaturesChooseSliderSettings());
	} else if (window.matchMedia("screen and (max-width: 1024px)").matches && groupCreated === true) {
		// groupCreated = false;

		// var groupSlider = $('.features-choose__slider');
		// var createdGroups = $('.features-choose__slider-group');

		// createdGroups.each(function(index, el) {
		// 	var lengthGroup = $(el).find('.features-choose__slider-item').length;
		// 	for (var i = 0; i < lengthGroup; i++) {
		// 		var elementSlides = $(el).find('.features-choose__slider-item');
		// 		var arrayElementSlides = elementSlides.toArray();
		// 		groupSlider.append(arrayElementSlides.shift());
		// 	}
		// });
	}
}

function healthCenterBox() {
	var healthSection = $('.health-center');
	var healthSectionWidth = $('.health-center').outerWidth();

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		healthSection.css({
			'padding-left': 150 + (($(window).width() - 1200) / 2) + 'px',
			'margin-right': 15 + (($(window).width() - 1200) / 2) + 'px'
		});
	} else {
		healthSection.attr('style', '');
	}
}

function checkDataPicker() {
	if ($('body').find('.ui-datepicker').length > 0) {
		setTimeout(function() {
			$('body').addClass('datepicker-on');
		}, 1000);
	}
}

function addWidthNutrLeft() {
	var enrollNutrLeft = $('.enroll-nutritionist__left');
	var enrollNutrLeftWidth;

	if (window.matchMedia("screen and (min-width: 1520px)").matches) {
		enrollNutrLeft.css('margin-left', (($(window).width() - 1520) / 2) + 'px');
	} else {
		enrollNutrLeft.css('margin-left', '0');
	}

	if (window.matchMedia("screen and (min-width: 1520px)").matches) {
		enrollNutrLeft.css('width', '460px');
	} else if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		enrollNutrLeft.css('width', 300 + (($(window).width() - 1200)/2) + 'px');
	}
}

function leftPositionDotsTopSlider() {

	var dotsLeftPosition   = 0,
			$slideContentWidth = $('.top-slider__item-content').outerWidth();

	if (window.matchMedia("screen and (min-width: 1920px)").matches) {

		dotsLeftPosition = $slideContentWidth + 30 + 360;

	} else if (window.matchMedia("screen and (min-width: 1200px)").matches) {

		dotsLeftPosition = $slideContentWidth + 30 + (($(window).width() - 1200) / 2);

	} else if (window.matchMedia("screen and (min-width: 768px)").matches) {
		dotsLeftPosition = $slideContentWidth + 30;

	} else {
		dotsLeftPosition = 15;
	}

	//Set slick-dots left position
	$('.top-slider .slick-dots').css('left', dotsLeftPosition + 'px');
}

function marginLeftSlideContentTopSlider() {
	var marginLeftSlideContent = 0;

	if (window.matchMedia("screen and (min-width:1920px)").matches) {

		marginLeftSlideContent = 360;

	} else if (window.matchMedia("screen and (min-width:1200px)").matches) {

		marginLeftSlideContent = ($(window).width() - 1200) / 2;

	} else if (window.matchMedia("screen and (min-width:768px)").matches) {

		marginLeftSlideContent = 0;

	}

	$('.top-slider__item-content').css('margin-left', marginLeftSlideContent + 'px');
}

function rightPositionArrowsTopSlider() {
	var rightPositionPrev = 0,
			rightPositionNext = 0;

	if (window.matchMedia("screen and (min-width:1920px)").matches) {

		rightPositionPrev = 68 + 360;

		rightPositionNext = 15 + 360;

	} else if (window.matchMedia("screen and (min-width:1200px)").matches) {

		rightPositionPrev = 68 + (($(window).width() - 1200) / 2);

		rightPositionNext = 15 + (($(window).width() - 1200) / 2);

	} else {

		rightPositionPrev = 68;

		rightPositionNext = 15;

	}

	$('.top-slider .slick-prev').css('right', rightPositionPrev + 'px');
	$('.top-slider .slick-next').css('right', rightPositionNext + 'px');
}

function noScrollBody(nameAttr, fixed) {
	var body = document.querySelector('body');
	var regModal = /modal/;
	var regModalChoice = /modal--choice/;
	var regModalCreateRacion= /modal--create-racion/;

	if (nameAttr.search(regModalCreateRacion) >= 0) {
		if (body.classList.contains('no-scroll')) {
			body.classList.remove('no-scroll');
		}
	} else if (mediaDownLG) {

		if (nameAttr.search(regModalChoice) >= 0 || fixed === 'not-fixed') {

		} else if (nameAttr.search(regModal) >= 0) {
			setTimeout(function() {
				if (body.classList.contains('no-scroll')) {
					body.classList.remove('no-scroll');
				} else {
					body.classList.add('no-scroll');
				}
			}, 150);
		}
	}
}

function calculateModalIndents(nameAttr) {
	var regModal = /modal/;
	// var regModalPR = /modal--menu-personal/;

	// if (nameAttr.search(regModalPR) >= 0) {

	// } else
	if (nameAttr.search(regModal) >= 0) {
		var modal = $(nameAttr);
		var modalBlock = modal.find('.modal__block');
		var modalBg = modal.find('.modal__bg');
		var heightDocument =  document.documentElement.clientHeight;
		var resHeight = heightDocument - 40;

		setTimeout(function() {
			var modalHeight = modalBlock[0].offsetHeight;
			var difference = resHeight - modalHeight;

			if (difference < 0) {
				modalBlock.css('margin-top', -(difference / 2) + 'px');
				modalBg.css('bottom', difference + 'px');
			}
		}, 100);
	}
}

function setDisplayBlock(selector) {
	var element = document.querySelector(selector);

	if (element) {

		if (element.classList.contains('active')) {
			element.style.display = 'block';
		} else {
			setTimeout(function() {
				element.style.display = 'none';
			}, 600);
		}
	}
}

function setDataToggle() {
	var btn = $('.menu-personal-block__btn');
	var btnModal = $('#modal__add-racion-js');

	if (mediaUpLGRacion) {

		btn.attr({
			'data-toggle': 'dropdown',
			'aria-haspopup': true,
			'aria-expanded': false,
												'data-flip': false
		});

		btnModal.attr({
			'data-toggle': 'dropdown',
			'aria-haspopup': true,
			'aria-expanded': false,
			'data-flip': false
		});

	} else if (mediaDownLGRacion) {
		btn.removeAttr('data-toggle aria-haspopup aria-expanded');
		btnModal.removeAttr('data-toggle aria-haspopup aria-expanded');
	}
}

function calculateKkalMenuPersonal(kkalItem, totalKkal, state, numDay) {
	totalKkalValue = totalKkal.textContent;

	if (mediaDownLGRacion) {
		var normBlock = document.querySelector('.racion__intake[current-day=\"' + numDay + '\"] .racion__intake-day-norm');
	} else if (mediaUpLGRacion) {
		var activeSlide= document.querySelector('.racion .swiper-slide-active');
		var normBlock = activeSlide.querySelector('.racion__intake-day-norm');
		var alertDayIntake = activeSlide.querySelector('.racion__intake-day-alert');
	}

	if (state === 'remove') {
		totalKkalValue = +totalKkalValue + +kkalItem;
	} else if (state === 'add') {
		totalKkalValue = +totalKkalValue - +kkalItem;
	}

	if (totalKkalValue < 0) {

		if (mediaDownLG) {
			var modalNormUp = document.querySelector('.modal--mobile-norm-up');
			var modalNormUpMeasure = modalNormUp.querySelector('.modal__personal-value');
			modalNormUpMeasure.textContent = totalKkalValue.toString().slice(1);
			$(modalNormUp).fadeIn(400);

		} else if (mediaUpLGRacion) {

			if (!alertDayIntake.classList.contains('visible')) {
				alertDayIntake.classList.add('visible');
			}
		}

		if (!normBlock.classList.contains('danger')) {
			normBlock.classList.add('danger');
		}

	} else if (totalKkalValue >= 0) {

		if (mediaUpLGRacion) {

			if (alertDayIntake.classList.contains('visible')) {
				alertDayIntake.classList.remove('visible');
			}
		}

		if (normBlock.classList.contains('danger')) {
			normBlock.classList.remove('danger');
		}
	}

	totalKkal.textContent = totalKkalValue;

	var popupDays = document.querySelector('.modal__days-food[data-current-day=\"' + numDay + '\"]'),
	kkal = popupDays.querySelector('.modal__days-kkal-value');

	kkal.textContent = totalKkalValue;
}

function calculateCostMenuPersonal(costItem, totalCost, state) {
	var totalCostValue = totalCost.textContent;
	var priceSelector = document.querySelectorAll('.racion__intake-add-price-value');
	var price = 0;

	if (state === 'add') {
		totalCost.textContent = +totalCostValue + +costItem;
	} else if (state === 'remove') {
		totalCost.textContent = +totalCostValue - +costItem;
	}

		//var dayNum = totalCost.closest('[current-day]').getAttribute('current-day');
	var dayNum = $(totalCost).parents('[current-day]').attr('current-day');
	var popupDay = document.querySelector('.modal__days-food[data-current-day=\"' + dayNum + '\"]');

	popupDay.querySelector('.modal__days-price-total-value').textContent = totalCost.textContent;

	calculateCostMenuPersonalByDay();
}

function calculateCostMenuPersonalByDay() {
	var priceSelector = document.querySelectorAll('.racion__intake-add-price-value');
	var price = 0;

	// общая цена за все дни
	for(var i = 0; i < priceSelector.length; i++) {
			price += +priceSelector[i].textContent;
	}

	document.querySelector('.modal__racion-total-value').textContent = price;
}

function calculateCostMenuPersonalByDish() {
	var days = document.querySelectorAll('.modal__days-item-header'),
			day = document.querySelectorAll('.modal__days-food'),
			dishes, totalPrice, price;

	for(var i = 0; i < days.length; i++) {
		dishes = day[i].querySelectorAll('.modal__food-dishes-item');

		totalPrice = 0;

		for(var j = 0; j < dishes.length; j++) {
				price = dishes[j].querySelector('.modal__dishes-price-value').textContent;

				totalPrice += +price;
		}

		day[i].querySelector('.modal__days-price-total-value').textContent = totalPrice;
	}

	calculateCostMenuPersonalByDay();
}

function showCreateRacionModal(animationTime) {
	var modalControllers = document.querySelectorAll('[data-racion-animation-show]');
	var body = document.querySelector('body');

	for (var i = 0; i < modalControllers.length; i++) {

		modalControllers[i].addEventListener('click', function() {
			var attrModalControllers = this.dataset.racionAnimationShow;
			var modal = document.querySelector(attrModalControllers);

			if (!modal.classList.contains('active')) {
				modal.classList.add('active');

				if (!body.classList.contains('no-scroll-menu-personal')) {
					setTimeout(function() {
						body.classList.add('no-scroll-menu-personal');
					}, animationTime);
				}
			}

			calculateRacionPopupBgHeight();
		});
	}
}

function hideCreateRacionModal(animationTime) {
	var modalControllers = document.querySelectorAll('[data-racion-animation-hide]');
	var body = document.querySelector('body');

	for (var i = 0; i < modalControllers.length; i++) {
		modalControllers[i].addEventListener('click', function() {
			var attrModalControllers = this.dataset.racionAnimationHide;
			var modal = document.querySelector(attrModalControllers);

			if (modal.classList.contains('active')) {
				modal.classList.remove('active');

				if (body.classList.contains('no-scroll-menu-personal')) {
					setTimeout(function() {
						body.classList.remove('no-scroll-menu-personal');
					}, 0);
				}
			}
		});
	}
}

function addToRacionFromModal() {
	var items = $('.menu-personal-block__item-body');

	items.each(function(index, el) {
		$(el).click(function(event) {
			var currentDay = $('.racion .swiper-slide-active');
			var intakes = currentDay.find('.racion__intake');
			intakes.removeClass('racion__intake--active');

			var catalogItem = $(el).parents('.menu-personal-block__item');
			var idParentItem = catalogItem.data('id');

			$.ajax({
								type: 'POST',
				url: $(this).attr('data-href'),
				dataType: 'json',
				data: {id:idParentItem},
				success: function(result) {
					var object = result[idParentItem];
					selectedItem.itemId = object.itemId;
					selectedItem.weight = object.weight;
					selectedItem.kkal = object.kkal;
					selectedItem.price = object.price;
					selectedItem.proteins = object.proteins;
					selectedItem.fat = object.fat;
					selectedItem.carbohydrates = object.carbohydrates;
					selectedItem.miniImg = object.miniImg;
					selectedItem.name = object.name;
					selectedItem.text = object.text;

					document.querySelector('[data-bimodal-img]').setAttribute('src',object.bigImg);
					document.querySelector('[data-bimodal-name]').textContent = object.name;
					document.querySelector('[data-bimodal-weigth]').textContent = object.weight + ' г';
					document.querySelector('[data-bimodal-kkal]').textContent = object.kkal + ' kkal';
					document.querySelector('[data-bimodal-text]').textContent = object.text;
					document.querySelector('[data-bimodal-proteins]').textContent = object.proteins + ' г.';
					document.querySelector('[data-bimodal-fat]').textContent = object.fat + ' г.';
					document.querySelector('[data-bimodal-carbohydrates]').textContent = object.carbohydrates + ' г.';
					document.querySelector('[data-bimodal-price]').textContent = object.price;
				}
			});
		});
	});
}

function checkDaysInWeeks() {
	var days = 0;
	var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
	var numIntakes = inputRacion.value;

	if (mediaUpLGRacion) {
		var currentDays = $('.racion__intake-day');
		days = currentDays.length;

	} else if (mediaDownLGRacion) {
		var currentIntakes = document.querySelectorAll('.racion__intake');

		if (currentIntakes.length > 0) {
				days = (currentIntakes.length / numIntakes);
		}
	}

	var subDay = 0;
	var subWeek = Math.ceil(days / 7);

	if (subWeek > 1) {
		subDay = days - (7 * (subWeek - 1));
	} else if (subWeek === 1) {
		subDay = days;
	}

	var modal = document.getElementById('modal--menu-personal-js');
	var modalWeeks = modal.querySelector('.modal__weeks');
	var numCurrentWeek = Math.floor(days / 7);

	if (subDay === 7) {

	}
}

function removeDishesItemPopup(day, eatTime, indexElem) {
	var modal = document.getElementById('modal--menu-personal-js');
	var listFoodsItem = modal.querySelector('.modal__days-food[data-current-day=\"' + day + '\"]');
	var foodsItem = listFoodsItem.querySelector('.modal__days-food-item[eat-time=\"' + eatTime + '\"]');
	var foodsItemListDishes = foodsItem.querySelector('.modal__food-dishes');
	var foodsItemDishes = foodsItemListDishes.children;

	for (var i = 0; i < foodsItemDishes.length; i++) {

		if (i === indexElem) {
			foodsItemListDishes.removeChild(foodsItemDishes[i]);
		}
	}

	if (foodsItemListDishes.children.length === 1) {

		if (!foodsItem.classList.contains('modal__days-food-item--empty')) {
			foodsItem.classList.add('modal__days-food-item--empty');
		}
	}
}

function removeDishesItem(btn) {

	btn.addEventListener('click', function() {
		var elemRemove = this.parentNode.parentNode;
		var parentRemove = elemRemove.parentNode;
		var swiperSlide = parentRemove.parentNode.parentNode;
		var swiperSlider = swiperSlide.parentNode.parentNode;
		var idParentItem = elemRemove.dataset.id;

		var elementsRemove = parentRemove.children;
		var indexElemRemove = Array.prototype.indexOf.call(elementsRemove, elemRemove);
		var intakeRemove = parentRemove.parentElement;
		var intakeRemoveEatTime = intakeRemove.getAttribute('eat-time');

		if (mediaDownLGRacion) {
			var intakeRemoveCurrentDay = intakeRemove.getAttribute('current-day');
		} else if (mediaUpLGRacion) {
			var intakeRemoveCurrentDay = intakeRemove.parentElement.getAttribute('current-day');
		}


		$.ajax({
			url: urlJson,
			dataType: 'json',
			data: {id:idParentItem},
			success: function(result) {
				var object = result[idParentItem];
				selectedItem.itemId = object.itemId;
				selectedItem.weight = object.weight;
				selectedItem.kkal = object.kkal;
				selectedItem.price = object.price;
				selectedItem.proteins = object.proteins;
				selectedItem.fat = object.fat;
				selectedItem.carbohydrates = object.carbohydrates;
				selectedItem.miniImg = object.miniImg;
				selectedItem.name = object.name;

				if (mediaDownLGRacion) {
					moveToSlideRacion(swiperSlide, racionSwiper);
					changeActiveRacionAttr(elemRemove);
					var racionActive = document.querySelectorAll('[active-add-racion="true"]');
					var dayNorm = racionActive[0].querySelector('.racion__intake-day-value');
					var totalCost = racionActive[racionActive.length - 1].querySelector('.racion__intake-add-price-value');
				} else if (mediaUpLGRacion) {
					var activeSlide = document.querySelector('.racion .swiper-slide-active');
					var dayNorm = activeSlide.querySelector('.racion__intake-day-value');
					var totalCost = activeSlide.querySelector('.racion__intake-add-price-value');
				}

				calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'remove', intakeRemoveCurrentDay);
				calculateCostMenuPersonal(selectedItem.price, totalCost, 'remove');

				parentRemove.removeChild(elemRemove);
				setTimeout(function() {
					racionSwiper.update(true);
				}, 100);

				if (mediaUpLGRacion) {
					calculateRacionPopupBgHeight();
				}

				removeDishesItemPopup(intakeRemoveCurrentDay, intakeRemoveEatTime, indexElemRemove);
			}
		});
	});
}

function changeActiveRacionAttr(item) {

	if (mediaDownLGRacion) {
		var selectedItemCurrentDay = item.parentElement.parentElement.getAttribute('current-day');
		var intakeAttrActiveRacion = document.querySelector('[active-add-racion="true"]');
		var currentDayActiveElement = intakeAttrActiveRacion.getAttribute('current-day');

		if (selectedItemCurrentDay != currentDayActiveElement) {

			var allIntakes = document.querySelectorAll('.racion__intake');
			for (var i = 0; i < allIntakes.length; i++) {
				if (allIntakes[i].getAttribute('current-day') === selectedItemCurrentDay) {
					allIntakes[i].setAttribute('active-add-racion', true);
				} else {
					allIntakes[i].setAttribute('active-add-racion', false);
				}
			}
		}
	}
}

function removeIntakeActiveClass() {
	var intakes = document.querySelectorAll('.racion__intake');

	for (var i = 0; i < intakes.length; i++) {

		if (intakes[i].classList.contains('racion__intake--active')) {
			intakes[i].classList.remove('racion__intake--active');
		}

		if (mediaUpLGRacion) {

			if (intakes[i].previousElementSibling.classList.contains('racion__intake--next-active')) {
				intakes[i].previousElementSibling.classList.remove('racion__intake--next-active');
			}
		}
	}
}

function clickDishesFromCarousel(item) {

	item.addEventListener('click', function() {

		if (!this.classList.contains('racion__dishes-item--not-empty')) {
			removeIntakeActiveClass();
			item.parentElement.parentElement.classList.add('racion__intake--active');
			changeActiveRacionAttr(item);
			checkActiveDishesSection = true;

			if (mediaUpLGRacion) {
				var prevItem = item.parentElement.parentElement.previousElementSibling;

				if (prevItem) {

					if (!prevItem.classList.contains('racion__intake--next-active')) {
						prevItem.classList.add('racion__intake--next-active');
					}
				}
			}
		}
	});
}
//e02
function createModalDay(days) {
	var subDay = 0;
	var subWeek = Math.ceil(days / 7);

	if (subWeek > 1) {
		subDay = days - (7 * (subWeek - 1));
	} else if (subWeek === 1) {
		subDay = days;
	}

	var modal = document.getElementById('modal--menu-personal-js');
	var modalWeeks = modal.querySelector('.modal__weeks');
	var numCurrentWeek = Math.floor(days / 7);

	if (numCurrentWeek > 0 && subDay === 1) {
		var justWeekHeader = modalWeeks.querySelectorAll('.modal__weeks-item-header');
		var justWeekDays = modalWeeks.querySelectorAll('.modal__days');

		var cloneJustWeekHeader = justWeekHeader[numCurrentWeek - 1].cloneNode(true);
		var cloneJustWeekDays = justWeekDays[numCurrentWeek - 1].cloneNode(true);

		if (cloneJustWeekHeader.classList.contains('ui-state-active')) {
			cloneJustWeekHeader.classList.remove('ui-state-active');
		}

		var keyCloneJustWeekHeader = cloneJustWeekHeader.querySelector('.modal__weeks-item-header-key');
		keyCloneJustWeekHeader.textContent = 'Неделя ' + (numCurrentWeek + 1);

		var headers = cloneJustWeekDays.querySelectorAll('.modal__days-item-header');
		var daysFood = cloneJustWeekDays.querySelectorAll('.modal__days-food');

		for (var i = 0; i < headers.length; i++) {

			if (i !== 0) {
				cloneJustWeekDays.removeChild(headers[i]);
				cloneJustWeekDays.removeChild(daysFood[i]);
			} else {
				daysFood[i].dataset.currentDay = days;
				var daysFoodItems = daysFood[i].querySelectorAll('.modal__days-food-item');
				var btnsCloneDayFoodItem = daysFood[i].querySelectorAll('.modal__days-food-item .modal__food-dishes-btn');

				for (var m = 0; m < btnsCloneDayFoodItem.length; m++) {
					addDishesFromPopup(btnsCloneDayFoodItem[m]);
				}

				for (var k = 0; k < daysFoodItems.length; k++) {
					var cloneDayfoodDishes = daysFoodItems[k].querySelector('.modal__food-dishes');
					var cloneDayfoodDishesItems = cloneDayfoodDishes.querySelectorAll('.modal__food-dishes-item');

					for (var j = 0; j < cloneDayfoodDishesItems.length; j++) {

						if (!cloneDayfoodDishesItems[j].classList.contains('modal__food-dishes-item--default')) {
							cloneDayfoodDishes.removeChild(cloneDayfoodDishesItems[j]);
						}
					}
				}

				if (headers[i].classList.contains('ui-state-active')) {
					headers[i].classList.remove('ui-state-active');
				}
			}
		}

		modalWeeks.append(cloneJustWeekHeader, cloneJustWeekDays);
		$(modalWeeks).accordion('refresh');

		var lastDays = modalWeeks.querySelectorAll('.modal__days');

		for (var i = 0; i < lastDays.length; i++) {

			if (i === lastDays.length - 1) {
				$(lastDays[i]).accordion({
					active: false,
					collapsible: true,
					header: '> .modal__days-item-header',
					heightStyle: 'content',
					icons: false,
					activate: function(event, ui) {

						if (mediaUpLGRacion) {
							var modalBlock = $('.modal--menu-personal .modal__block');
							var modalHeight = modalBlock.outerHeight();
							var res = modalCreateRacionBgHeight + 17 - modalHeight;

							if (res < 0) {
								modalCreateRacionBg.css('bottom', res + 'px');
							} else if (res >= 0) {
								modalCreateRacionBg.css('bottom', 0 + 'px');
							}
						}
					}
				});
			}
		}

		var modalDays = document.querySelectorAll('.modal__days');
		var btnModalDays = modalDays[numCurrentWeek - 1].lastElementChild;
		modalDays[numCurrentWeek - 1].removeChild(btnModalDays);
		addNewDay(document.getElementById('add-day-menu-personal-js'));
	}

	var weeksHeader = document.querySelectorAll('#modal--menu-personal-js .modal__weeks-item-header');

	if (subDay === 7) {
		numCurrentWeek = numCurrentWeek - 1;
	}

		var currentWeekHeader = weeksHeader[numCurrentWeek];
		var currentModalDays = currentWeekHeader.nextElementSibling;
		var dayHeader = currentModalDays.querySelector('.modal__days-item-header');
		var daysFood = currentModalDays.querySelector('.modal__days-food');
		var btnNewday = currentModalDays.lastElementChild;
		var cloneDayHeader = dayHeader.cloneNode(true);
		var btnRemoveDay = cloneDayHeader.querySelector('.modal__food-dishes-item-btn--day');

	if (subDay != 1) {

				btnRemoveDay.addEventListener('click', function() {
						removeDayPopup(this);
				});

		var cloneDayFood = daysFood.cloneNode(true);
		cloneDayFood.dataset.currentDay = days;
		var nameCloneDay = cloneDayHeader.querySelector('.modal__days-item-header-key');
		nameCloneDay.textContent = 'День ' + subDay;

		var cloneDayFoodItems = cloneDayFood.querySelectorAll('.modal__days-food-item');
		var btnsCloneDayFoodItem = cloneDayFood.querySelectorAll('.modal__days-food-item .modal__food-dishes-btn');

		for (var i = 0; i < btnsCloneDayFoodItem.length; i++) {
			addDishesFromPopup(btnsCloneDayFoodItem[i]);
		}

		for (var i = 0; i < cloneDayFoodItems.length; i++) {
			var cloneDayfoodDishes = cloneDayFoodItems[i].querySelector('.modal__food-dishes');
			var cloneDayfoodDishesItems = cloneDayfoodDishes.querySelectorAll('.modal__food-dishes-item');

			for (var j = 0; j < cloneDayfoodDishesItems.length; j++) {

				if (!cloneDayfoodDishesItems[j].classList.contains('modal__food-dishes-item--default')) {
					cloneDayfoodDishes.removeChild(cloneDayfoodDishesItems[j]);
				}
			}
		}

		if (cloneDayHeader.classList.contains('ui-state-active')) {
			cloneDayHeader.classList.remove('ui-state-active');
		}

		currentModalDays.insertBefore(cloneDayHeader, btnNewday);
		currentModalDays.insertBefore(cloneDayFood, btnNewday);
	} else if(subDay == 1) {
		var btnRemoveDay = dayHeader.querySelector('.modal__food-dishes-item-btn--day');

		btnRemoveDay.addEventListener('click', function() {
				removeDayPopup(this);
		});
	}

	$(currentModalDays).accordion('refresh');

	if (mediaUpLGRacion) {
		calculateRacionPopupBgHeight();
	}
}

function addNewDay(btn) {

	btn.addEventListener('click', function() {
		var numOfDays = 0;
		var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
		var numIntakes = inputRacion.value;

		if (mediaUpLGRacion) {
			var currentDays = $('.racion__intake-day');
			numOfDays = currentDays.length + 1;

		} else if (mediaDownLGRacion) {
			var currentIntakes = document.querySelectorAll('.racion__intake');

			if (currentIntakes.length > 0) {
				numOfDays = (currentIntakes.length / numIntakes) + 1;
			}
		}

		createIntakes(numOfDays);
		checkActiveDishesSection = false;
		removeIntakeActiveClass();
		hideIntakes(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));
		hideModalBullet(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));

		if (window.matchMedia("screen and (min-width: 1200px)").matches) {
			disableDroppableAreas();
		} else if (mediaDownLGRacion) {
			setTimeout(function() {
				racionSwiper.slideTo((currentIntakes.length));
			}, 100);
		}
		createModalDay(numOfDays);
		checkBtnRemoveDay();
		checkBtnDoubleDay();
		checkBtnsDoubleWeek();

		btnsDoubleDay();
	});
}

function checkBtnDoubleDay() {
	var modalPopup = document.getElementById('modal--menu-personal-js');

	if (modalPopup) {
		var headers = modalPopup.querySelectorAll('.modal__days-item-header');

		if (headers.length === 1) {

			var btn = headers[0].querySelector('.modal__double-btn_day');

			if (!btn.classList.contains('hidden')) {
				//btn.classList.add('hidden');
			}
		} else if (headers.length > 1) {

			var weeks = modalPopup.querySelectorAll('.modal__weeks-item-header');

			if(weeks.length === 1) {

				for (var i = 0; i < headers.length; i++) {
					var btn = headers[i].querySelector('.modal__double-btn_day');

					if (btn.classList.contains('hidden')) {
						btn.classList.remove('hidden');
					}
				}
			} else {
				for (var i = 0; i < headers.length; i++) {
					var btn = headers[i].querySelector('.modal__double-btn_day');
					btn.classList.add('hidden');
				}

				headers = modalPopup.querySelector('.modal__days:last-child').querySelectorAll('.modal__days-item-header');

				for (var i = 0; i < headers.length; i++) {
					var btn = headers[i].querySelector('.modal__double-btn_day');

					if (btn.classList.contains('hidden')) {
						btn.classList.remove('hidden');
					}
				}
			}
		}
	}
}

function checkBtnsDoubleWeek() {
    var btns = document.querySelectorAll('.modal__double-btn_week');

    for(var i = 0; i < btns.length; i++) {
        var listDays = btns[i].parentElement.nextElementSibling;
        var headers = listDays.querySelectorAll('.modal__days-item-header');

        btnDoubleWeek(btns[i]);

        if(headers.length >= 7 && btns[i].classList.contains('hidden')) {
            btns[i].classList.remove('hidden');
        } else if(headers.length < 7 && !btns[i].classList.contains('hidden')) {
            btns[i].classList.add('hidden');
        }
    }
}

function doubleIntakesWeek(dayNumber, doubleDay, afterDay) {

    if(mediaUpLGRacion) {
        var slider = document.querySelector('.swiper-wrapper'),
            afterSlide = slider.querySelector('.swiper-slide[current-day=\"' + afterDay + '\"]'),
            slide = slider.querySelector('.swiper-slide[current-day=\"' + doubleDay + '\"]'),
            slideClone = slide.cloneNode(true),
            slides,
            dishesBtnDel = slideClone.querySelectorAll('.racion__dishes-btn-del'),
            intakeBtnDel = slideClone.querySelectorAll('.racion__intake-addition-btn'),
            dishesItem = slideClone.querySelectorAll('.racion__dishes-item'),
            intake = slideClone.querySelectorAll('.racion__intake');

        $(afterSlide).after(slideClone);

        slides = slider.querySelectorAll('.swiper-slide');

        for(var i = 0; i < slides.length; i++) {
            slides[i].setAttribute('current-day',(i + 1));
            slides[i].querySelector('.racion__intake-day-title').textContent = "День " + (i + 1);
        }

        for(var i = 0; i < dishesBtnDel.length; i++) {
            removeDishesItem(dishesBtnDel[i]);
        }

        for(var i = 0; i < intakeBtnDel.length; i++) {
            сancellationDishes(intakeBtnDel[i]);
        }

        for(var i = 0; i < dishesItem.length; i++) {
            clickDishesFromCarousel(dishesItem[i]);
        }

        for(var i = 0; i < intake.length; i++) {
            dropMenuPersonal(intake[i]);
        }

        recalculateRacionSwiper();

        var prevBtnSlider = $('.racion .swiper-button-prev');
        var nextBtnSlider = $('.racion .swiper-button-next');

        prevBtnSlider.addClass('swiper-button--visible');
        nextBtnSlider.addClass('swiper-button--visible');
    }

    if(mediaDownLGRacion) {
        var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
	var numIntakes = inputRacion.value;

        var slider = document.querySelector('.swiper-wrapper'),
            afterSlides = slider.querySelectorAll('.racion__intake[current-day=\"' + afterDay + '\"]'),
            slides = slider.querySelectorAll('.racion__intake[current-day=\"' + doubleDay + '\"]'),
            lastSlide = slides[slides.length - 1].parentElement,
            allSlides,
            laSlide = afterSlides[afterSlides.length -1].parentElement;

        for(var i = 0; i < slides.length; i++) {
            var clone = slides[i].parentElement.cloneNode(true),
                dishesBtnDel = clone.querySelector('.racion__dishes-btn-del'),
                dishesItem = clone.querySelector('.racion__dishes-item');

            $(laSlide).after(clone);
            lastSlide = clone;
            laSlide = clone;

            removeDishesItem(dishesBtnDel);
            clickDishesFromCarousel(dishesItem);
        }

        allSlides = slider.querySelectorAll('.swiper-slide');

        for(var i = 0, j = 0; i < allSlides.length; i++) {
            var intake = allSlides[i].querySelector('.racion__intake');

            if(i % numIntakes == 0) {
                j++;

                intake.querySelector('.racion__intake-day-title').textContent = "День " + j;
            }

            intake.setAttribute('current-day', j);
        }

        //doubleIntakeBlockLI();
        //addNewDay(document.querySelector('.racion__intake-add-btn'));
    }

    racionSwiper.update(true);
}


function btnsDoubleDay() {
		var btns = document.querySelectorAll('.modal__double-btn_day');

		for(var i = 0; i < btns.length; i++) {
				btns[i].onclick = function(event){
						var btn = $(this);
						var parentItem = this.parentElement;
						var parentItemClone = parentItem.cloneNode(true);
						var ulItem = parentItem.nextElementSibling;
						var ulItemClone = ulItem.cloneNode(true);
						var newBtn = parentItemClone.querySelector('.modal__double-btn_day');
						var doubleDay = ulItem.getAttribute('data-current-day');

						var newDayBtn = document.getElementById('add-day-menu-personal-js');

						var numOfDays = 0;
						var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
						var numIntakes = inputRacion.value;

						btn.parents('.modal__days').one("accordionbeforeactivate", function(event, ui) {
								return false;
						});

						if (mediaUpLGRacion) {
								var currentDays = $('.racion__intake-day');
								numOfDays = currentDays.length + 1;

						} else if (mediaDownLGRacion) {
								var currentIntakes = document.querySelectorAll('.racion__intake');

								if (currentIntakes.length > 0) {
										numOfDays = (currentIntakes.length / numIntakes) + 1;
								}
						}

						doubleIntakes(numOfDays, doubleDay);
						checkActiveDishesSection = false;
						removeIntakeActiveClass();
						hideIntakes(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));
						hideModalBullet(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));

						var subDay = 0;
						var subWeek = Math.ceil(numOfDays / 7);

						if (subWeek > 1) {
										subDay = numOfDays - (7 * (subWeek - 1));
						} else if (subWeek === 1) {
										subDay = numOfDays;
						}

						var modal = document.getElementById('modal--menu-personal-js');
						var modalWeeks = modal.querySelector('.modal__weeks');
						var numCurrentWeek = Math.floor(numOfDays / 7);

						if (numCurrentWeek > 0 && subDay === 1) {
								var justWeekHeader = modalWeeks.querySelectorAll('.modal__weeks-item-header');
								var justWeekDays = modalWeeks.querySelectorAll('.modal__days');

		var cloneJustWeekHeader = justWeekHeader[numCurrentWeek - 1].cloneNode(true);
		var cloneJustWeekDays = justWeekDays[numCurrentWeek - 1].cloneNode(false);

		if (cloneJustWeekHeader.classList.contains('ui-state-active')) {
										cloneJustWeekHeader.classList.remove('ui-state-active');
		}

								var keyCloneJustWeekHeader = cloneJustWeekHeader.querySelector('.modal__weeks-item-header-key');
		keyCloneJustWeekHeader.textContent = 'Неделя ' + (numCurrentWeek + 1);

								cloneJustWeekDays.append(parentItemClone, ulItemClone, newDayBtn);
								ulItemClone.dataset.currentDay = numOfDays;

								if (parentItemClone.classList.contains('ui-state-active')) {
												parentItemClone.classList.remove('ui-state-active');
								}

								modalWeeks.append(cloneJustWeekHeader, cloneJustWeekDays);

								$(modalWeeks).accordion('refresh');

								var lastDays = modalWeeks.querySelectorAll('.modal__days');

								for (var i = 0; i < lastDays.length; i++) {

										if (i === lastDays.length - 1) {
												$(lastDays[i]).accordion({
														active: false,
														collapsible: true,
														header: '> .modal__days-item-header',
														heightStyle: 'content',
														icons: false,
														activate: function(event, ui) {

																if (mediaUpLGRacion) {
																		var modalBlock = $('.modal--menu-personal .modal__block');
																		var modalHeight = modalBlock.outerHeight();
																		var res = modalCreateRacionBgHeight + 17 - modalHeight;

																		if (res < 0) {
																				modalCreateRacionBg.css('bottom', res + 'px');
																		} else if (res >= 0) {
																				modalCreateRacionBg.css('bottom', 0 + 'px');
																		}
																}
														}
												});
										}
								}

		//var modalDays = document.querySelectorAll('.modal__days');
		//var btnModalDays = modalDays[numCurrentWeek - 1].lastElementChild;
		//modalDays[numCurrentWeek - 1].removeChild(btnModalDays);
		//addNewDay(document.getElementById('add-day-menu-personal-js'));

						} else {
								$(ulItem).after(parentItemClone, ulItemClone);
						}

						parentItemClone.querySelector('.modal__food-dishes-item-btn--day').addEventListener('click', function() {
								removeDayPopup(this);
						});


						var weekDays = modalWeeks.querySelectorAll('.modal__days');
						var ncweek = subDay == 7 ? numCurrentWeek -1 : numCurrentWeek;
						var days = weekDays[ncweek].querySelectorAll('.modal__days-item-header');

						for(var i = 0; i < days.length; i++) {
								var day = ncweek * 7,
										currentDay = (i + 1);

								if(day > 0) {
										currentDay = (i + 1) + day;
								}

								days[i].querySelector('.modal__days-item-header-key').textContent = "День " + (i + 1);
								days[i].nextElementSibling.dataset.currentDay = currentDay;
						}

						$('.modal__days').accordion('refresh');

						var btnsCloneDayFoodItem = ulItemClone.querySelectorAll('.modal__days-food-item .modal__food-dishes-btn');

						for (var i = 0; i < btnsCloneDayFoodItem.length; i++) {
								addDishesFromPopup(btnsCloneDayFoodItem[i]);
						}

						var btnsRemove = ulItemClone.querySelectorAll('.modal__food-dishes-item-btn');
						btnRemoveDishesItemPopup(btnsRemove);

						calculateCostMenuPersonalByDay();

						checkBtnRemoveDay();
						checkBtnDoubleDay();

						btnDoubleDay(newBtn);

						if (mediaUpLGRacion) {
								calculateRacionPopupBgHeight();
						}

						checkBtnsDoubleWeek();
				}
		}
}

function btnDoubleDay(button) {
		button.onclick = function(event){
				var btn = $(this);
				var parentItem = this.parentElement;
				var parentItemClone = parentItem.cloneNode(true);
				var ulItem = parentItem.nextElementSibling;
				var ulItemClone = ulItem.cloneNode(true);
				var newBtn = parentItemClone.querySelector('.modal__double-btn_day');
				var doubleDay = ulItem.getAttribute('data-current-day');

				var newDayBtn = document.getElementById('add-day-menu-personal-js');

				var numOfDays = 0;
				var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
				var numIntakes = inputRacion.value;

				btn.parents('.modal__days').one("accordionbeforeactivate", function(event, ui) {
						return false;
				});

				if (mediaUpLGRacion) {
						var currentDays = $('.racion__intake-day');
						numOfDays = currentDays.length + 1;

				} else if (mediaDownLGRacion) {
						var currentIntakes = document.querySelectorAll('.racion__intake');

						if (currentIntakes.length > 0) {
								numOfDays = (currentIntakes.length / numIntakes) + 1;
						}
				}

				doubleIntakes(numOfDays, doubleDay);
				checkActiveDishesSection = false;
				removeIntakeActiveClass();
				hideIntakes(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));
				hideModalBullet(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));

				var subDay = 0;
				var subWeek = Math.ceil(numOfDays / 7);

				if (subWeek > 1) {
								subDay = numOfDays - (7 * (subWeek - 1));
				} else if (subWeek === 1) {
								subDay = numOfDays;
				}

				var modal = document.getElementById('modal--menu-personal-js');
				var modalWeeks = modal.querySelector('.modal__weeks');
				var numCurrentWeek = Math.floor(numOfDays / 7);

				if (numCurrentWeek > 0 && subDay === 1) {
						var justWeekHeader = modalWeeks.querySelectorAll('.modal__weeks-item-header');
						var justWeekDays = modalWeeks.querySelectorAll('.modal__days');

						var cloneJustWeekHeader = justWeekHeader[numCurrentWeek - 1].cloneNode(true);
						var cloneJustWeekDays = justWeekDays[numCurrentWeek - 1].cloneNode(false);

						if (cloneJustWeekHeader.classList.contains('ui-state-active')) {
								cloneJustWeekHeader.classList.remove('ui-state-active');
						}

						var keyCloneJustWeekHeader = cloneJustWeekHeader.querySelector('.modal__weeks-item-header-key');
						keyCloneJustWeekHeader.textContent = 'Неделя ' + (numCurrentWeek + 1);

						cloneJustWeekDays.append(parentItemClone, ulItemClone, newDayBtn);
						ulItemClone.dataset.currentDay = numOfDays;

						if (parentItemClone.classList.contains('ui-state-active')) {
										parentItemClone.classList.remove('ui-state-active');
						}

						modalWeeks.append(cloneJustWeekHeader, cloneJustWeekDays);

						$(modalWeeks).accordion('refresh');

						var lastDays = modalWeeks.querySelectorAll('.modal__days');

						for (var i = 0; i < lastDays.length; i++) {

								if (i === lastDays.length - 1) {
										$(lastDays[i]).accordion({
												active: false,
												collapsible: true,
												header: '> .modal__days-item-header',
												heightStyle: 'content',
												icons: false,
												activate: function(event, ui) {

														if (mediaUpLGRacion) {
																var modalBlock = $('.modal--menu-personal .modal__block');
																var modalHeight = modalBlock.outerHeight();
																var res = modalCreateRacionBgHeight + 17 - modalHeight;

																if (res < 0) {
																		modalCreateRacionBg.css('bottom', res + 'px');
																} else if (res >= 0) {
																		modalCreateRacionBg.css('bottom', 0 + 'px');
																}
														}
												}
										});
								}
						}

						//var modalDays = document.querySelectorAll('.modal__days');
						//var btnModalDays = modalDays[numCurrentWeek - 1].lastElementChild;
						//modalDays[numCurrentWeek - 1].removeChild(btnModalDays);
						//addNewDay(document.getElementById('add-day-menu-personal-js'));

				} else {
						$(ulItem).after(parentItemClone, ulItemClone);
				}

				parentItemClone.querySelector('.modal__food-dishes-item-btn--day').addEventListener('click', function() {
						removeDayPopup(this);
				});

				var weekDays = modalWeeks.querySelectorAll('.modal__days');
				var ncweek = subDay == 7 ? numCurrentWeek -1 : numCurrentWeek;
				var days = weekDays[ncweek].querySelectorAll('.modal__days-item-header');

				for(var i = 0; i < days.length; i++) {
						var day = ncweek * 7,
								currentDay = (i + 1);

						if(day > 0) {
								currentDay = (i + 1) + day;
						}

						days[i].querySelector('.modal__days-item-header-key').textContent = "День " + (i + 1);
						days[i].nextElementSibling.dataset.currentDay = currentDay;
				}

				$('.modal__days').accordion('refresh');

				var btnsCloneDayFoodItem = ulItemClone.querySelectorAll('.modal__days-food-item .modal__food-dishes-btn');

				for (var i = 0; i < btnsCloneDayFoodItem.length; i++) {
								addDishesFromPopup(btnsCloneDayFoodItem[i]);
				}

				var btnsRemove = ulItemClone.querySelectorAll('.modal__food-dishes-item-btn');
				btnRemoveDishesItemPopup(btnsRemove);

				calculateCostMenuPersonalByDay();

				checkBtnRemoveDay();
				checkBtnDoubleDay();

				btnDoubleDay(newBtn);

				if (mediaUpLGRacion) {
						calculateRacionPopupBgHeight();
				}

				checkBtnsDoubleWeek();
		}
}

function btnDoubleWeek(button) {
    button.onclick = function() {
        var btn = $(this),
            header = this.parentElement,
            days = header.nextElementSibling,
            headerClone = header.cloneNode(true),
            daysClone = days.cloneNode(true);

        var numOfDays = 0;
        var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
        var numIntakes = inputRacion.value;

        var modal = document.getElementById('modal--menu-personal-js');
        var modalWeeks = modal.querySelector('.modal__weeks');

        btn.parents('.modal__weeks').one("accordionbeforeactivate", function(event, ui) {
            return false;
        });

        if (window.matchMedia("screen and (min-width: 992px)").matches) {
            var currentDays = $('.racion__intake-day');
            numOfDays = currentDays.length + 1;

        } else if (window.matchMedia("screen and (max-width: 991px)").matches) {
            var currentIntakes = document.querySelectorAll('.racion__intake');

            if (currentIntakes.length > 0) {
                numOfDays = (currentIntakes.length / numIntakes) + 1;
            }
        }

        var btnDelDay = daysClone.querySelectorAll('.modal__food-dishes-item-btn--day');

        for(var i = 0; i < btnDelDay.length; i++) {
            btnDelDay[i].addEventListener('click', function() {
                removeDayPopup(this);
            });
        }

        var newDays = days.querySelectorAll('.modal__days-food'),
            lastDay = newDays[newDays.length -1].dataset.currentDay;

        for(var i = 0; i < newDays.length; i++) {
            var doubleDay = newDays[i].dataset.currentDay;

            doubleIntakesWeek(numOfDays, doubleDay, lastDay);
            lastDay++;
        }

        var slides = document.querySelector('.swiper-wrapper').querySelectorAll('.swiper-slide');
        var btn = slides[slides.length -1].querySelector('.racion__intake-add-btn');

        if(btn) {
            addNewDay(btn);
        } else {
            var addIntakeBtn = document.createElement('button');
            addIntakeBtn.className = 'racion__intake-add-btn';
            addIntakeBtn.type = 'button';
            addIntakeBtn.append('Cоздать новый день');

            var orderIntakeBtn = document.createElement('button');
            orderIntakeBtn.className = 'racion__intake-order-btn';
            orderIntakeBtn.type = 'button';
            orderIntakeBtn.append('Заказать рацион');

            slides[slides.length -1].querySelector('.racion__intake-add-block').append(addIntakeBtn, orderIntakeBtn);

            addNewDay(addIntakeBtn);
        }

        doubleIntakeBlockLI();

        checkActiveDishesSection = false;
        removeIntakeActiveClass();
        hideIntakes(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));
        hideModalBullet(document.querySelector('.modal--menu-personal .field-radio__name--personal input:checked'));

        var btnAddNewDay = days.querySelector('#add-day-menu-personal-js');
        if(btnAddNewDay) {
            days.removeChild(days.querySelector('#add-day-menu-personal-js'));
        }

        $(days).after(headerClone, daysClone);

        $(modalWeeks).accordion('refresh');

        $(daysClone).accordion({
            active: false,
            collapsible: true,
            header: '> .modal__days-item-header',
            heightStyle: 'content',
            icons: false,
            activate: function(event, ui) {

                if (window.matchMedia("screen and (min-width: 992px)").matches) {
                    var modalBlock = $('.modal--menu-personal .modal__block');
                    var modalHeight = modalBlock.outerHeight();
                    var res = modalCreateRacionBgHeight + 17 - modalHeight;

                    if (res < 0) {
                        modalCreateRacionBg.css('bottom', res + 'px');
                    } else if (res >= 0) {
                        modalCreateRacionBg.css('bottom', 0 + 'px');
                    }
                }
            }
        });

        var headers = modalWeeks.querySelectorAll('.modal__weeks-item-header');
        var modalDays = modalWeeks.querySelectorAll('.modal__days-food');

        for(var i = 0; i < headers.length; i++) {
            headers[i].querySelector('.modal__weeks-item-header-key').textContent = "Неделя " + (i + 1);
        }

        for(var i = 0; i < modalDays.length; i++) {
            modalDays[i].dataset.currentDay = (i + 1);
        }

        var btnsCloneDayFoodItem = daysClone.querySelectorAll('.modal__days-food-item .modal__food-dishes-btn');
        var newBtn = daysClone.querySelectorAll('.modal__double-btn_day');
        var btnsRemove = daysClone.querySelectorAll('.modal__food-dishes-item-btn');

        for (var i = 0; i < btnsCloneDayFoodItem.length; i++) {
            addDishesFromPopup(btnsCloneDayFoodItem[i]);
        }

        btnRemoveDishesItemPopup(btnsRemove);

        calculateCostMenuPersonalByDay();

        checkBtnRemoveDay();
        checkBtnDoubleDay();

        for (var i = 0; i < newBtn.length; i++) {
            btnDoubleDay(newBtn[i]);
        }

        btnDoubleWeek(headerClone.querySelector('.modal__double-btn_week'));
    }
}

function сancellationDishes(btn) {

	$(btn).click(function(event) {
		removeIntakeActiveClass();
		checkActiveDishesSection = false;
	});
}

function toggleDropdownShow(btn, state) {
	// if (state === 'on') {

	// 	btn.parents('.menu-personal-block__item-bottom').one('show.bs.dropdown', function(event) {
	// 		return true;
	// 	});
	// } else if (state === 'off') {

	// 	btn.parents('.menu-personal-block__item-bottom').one('show.bs.dropdown', function(event) {
	// 		return false;
	// 	});
	// }

	if (state === 'off') {
		btn.dropdown('toggle');
	}
}

function selectItemRacionModalDescr() {
	$(document).on('click','#modal__add-racion-js',function(event) {
		var body = document.querySelector('body'),
										btn = $(this);

		if (body.classList.contains('no-scroll')) {
			body.classList.remove('no-scroll');
		}

		checkActiveDishesSection = false;

		var intakes = document.querySelectorAll('.racion__intake');
		for (var i = 0; i < intakes.length; i++) {

			if (intakes[i].classList.contains('racion__intake--active')) {
				intakes[i].classList.remove('racion__intake--active');
			}

			if (mediaUpLGRacion) {
				if (intakes[i].previousElementSibling.classList.contains('racion__intake--next-active')) {
					intakes[i].previousElementSibling.classList.remove('racion__intake--next-active');
				}
			}
		}

		if (mediaDownLGRacion) {
			btn.parents('.modal--menu-personal-block').fadeOut(400);
			var modal = $('.modal--choice');
			modal.fadeIn(400);
		} else if (mediaUpLGRacion) {

		}
	});
}

function addDishesToModal(dayNum, eatTime, dish) {
	var modal = document.getElementById('modal--menu-personal-js');
	var days = modal.querySelectorAll('.modal__days-food');
	for (var i = 0; i < days.length; i++) {
		var currentDay = days[i].dataset.currentDay;

		if (dayNum === currentDay) {
			var foods = days[i].children;

			for (var j = 0; j < foods.length; j++) {
				var foodEatTime = foods[j].getAttribute('eat-time');

				if (eatTime === foodEatTime) {
					var dishesContainer = foods[j].querySelector('.modal__food-dishes');
					var lastDishesItem = dishesContainer.lastElementChild;

					var daysFoodItem = dishesContainer.parentElement;
					daysFoodItem.classList.remove('modal__days-food-item--empty');

					var clone = lastDishesItem.cloneNode(true);
					clone.classList.remove('modal__food-dishes-item--default');
					clone.setAttribute('item-id', dish.itemId);

					var btnsRemove = clone.querySelectorAll('.modal__food-dishes-item-btn');

					var dishPropertyList = clone.querySelector('.modal__dishes-properties');
					var dishProperties = dishPropertyList.querySelectorAll('.modal__dishes-properties-item');

					//Set property value in clone
					for (var k = 0; k < dishProperties.length; k++) {
						var propName = dishProperties[k].dataset.proper;

						for (var key in dish) {

							if (key === propName) {
								var valuePropName = dishProperties[k].querySelector('.modal__dishes-properties-sense');
								valuePropName.textContent = dish[key];
							}
						}
					}

					//Set price value clone
					var priceValue = clone.querySelector('.modal__dishes-price-value');
										var nameValue = clone.querySelector('.modal__food-dishes-item-name');

					for (var key in dish) {

						if (key === 'price') {
							priceValue.textContent = dish[key];
						}
												if (key === 'name') {
							nameValue.querySelector('span').textContent = dish[key];
						}
					}
					dishesContainer.insertBefore(clone, lastDishesItem);

					btnRemoveDishesItemPopup(btnsRemove);
				}
			}
		}
	}
}

function btnRemoveDishesItemPopup(btns) {
	for (var i = 0; i < btns.length; i++) {

		btns[i].addEventListener('click', function() {
			var foodItem = $(this).parents('.modal__days-food-item');
			var foodItemList = $(this).parents('.modal__days-food');
			var foodDishes = $(this).parents('.modal__food-dishes');
			var foodDishesItem = $(this).parents('.modal__food-dishes-item');
			var idFoodDishesItem = foodDishesItem.attr('item-id');

			var indexEl = foodDishesItem.index();
			var eatTime = foodItem[0].getAttribute('eat-time');
			var currentDay = foodItemList[0].dataset.currentDay;

			$.ajax({
				url: urlJson,
				dataType: 'json',
				data: {id:idFoodDishesItem},
				success: function(result) {
					var object = result[idFoodDishesItem];
					selectedItem.itemId = object.itemId;
					selectedItem.weight = object.weight;
					selectedItem.kkal = object.kkal;
					selectedItem.price = object.price;
					selectedItem.proteins = object.proteins;
					selectedItem.carbohydrates = object.carbohydrates;
					selectedItem.fat = object.fat;
					selectedItem.miniImg = object.miniImg;
										selectedItem.name = object.name;

					removeDishesItemPopup(currentDay, eatTime, indexEl);
					removeDishesItemCarousel(currentDay, eatTime, indexEl, selectedItem);
					calculateRacionPopupBgHeight();

										calculateCostMenuPersonalByDish();
				}
			});
		});
	}
}

function clickRemoveDayPopup() {
	var modal = document.getElementById('modal--menu-personal-js');

	if (modal) {
		var btns = modal.querySelectorAll('.modal__food-dishes-item-btn--day');

		for (var i = 0; i < btns.length; i++) {

			btns[i].addEventListener('click', function() {
				removeDayPopup(this);
			});
		}
	}

	checkBtnRemoveDay();
	checkBtnDoubleDay();
}

function clickAddDishesBtnPopup() {
	var modalPopup = document.getElementById('modal--menu-personal-js');

	if (modalPopup) {
		var btns = modalPopup.querySelectorAll('.modal__days-food-item .modal__food-dishes-btn');

		for (var i = 0; i < btns.length; i++) {

			addDishesFromPopup(btns[i]);
		}
	}
}

function addDishesFromPopup(btn) {
	var modalPopup = document.getElementById('modal--menu-personal-js');
	var modal = document.getElementById('racion-js');
	var swiperWrapper = modal.querySelector('.swiper-wrapper');
	var body = document.querySelector('body');

	btn.addEventListener('click', function() {
		var containerBtn = this.parentElement;
		var daysFood = containerBtn.parentElement;
		var eatTime = containerBtn.getAttribute('eat-time');
		var currentDay = daysFood.dataset.currentDay;

		if (modalPopup.classList.contains('active')) {
			modalPopup.classList.remove('active');
		}

		if (body.classList.contains('no-scroll-menu-personal')) {
			body.classList.remove('no-scroll-menu-personal');
		}

		if (mediaDownLGRacion) {
			var intakes = modal.querySelectorAll('.swiper-slide .racion__intake');

			for (var i = 0; i < intakes.length; i++) {
				var currentDayRacion = intakes[i].getAttribute('current-day');
				var activeAddRacionAttr = intakes[i].getAttribute('active-add-racion');

				if (currentDayRacion === currentDay) {

					if (activeAddRacionAttr === 'false') {
						intakes[i].setAttribute('active-add-racion', true);
					}

					var eatTimeRacion = intakes[i].getAttribute('eat-time');

					if (eatTimeRacion === eatTime) {
						moveToSlideRacion(intakes[i].parentElement, racionSwiper);

						if (!intakes[i].classList.contains('racion__intake--active')) {
							intakes[i].classList.add('racion__intake--active');
						}

						checkActiveDishesSection = true;
					}
				} else {

					if (activeAddRacionAttr === 'true') {
						intakes[i].setAttribute('active-add-racion', false);
					}
				}
			}
		} else if (mediaUpLGRacion) {

			var swiperSlides = modal.querySelectorAll('.swiper-slide');

			for (var i = 0; i < swiperSlides.length; i++) {
				var slideCurrentDay = swiperSlides[i].getAttribute('current-day');

				if (slideCurrentDay === currentDay) {

					var intake = swiperSlides[i].querySelector('.racion__intake[eat-time=\"' + eatTime + '\"]');

					if (!intake.classList.contains('racion__intake--active')) {
						intake.classList.add('racion__intake--active');
					}

					checkActiveDishesSection = true;

					moveToSlideRacion(swiperSlides[i], racionSwiper);
				}
			}
		}
	});
}

function resetCurrentDayAttr() {
	var modalRacion = document.getElementById('racion-js');
	var modalPopup = document.getElementById('modal--menu-personal-js');

	if (mediaDownLGRacion) {
		var slides = modalRacion.querySelectorAll('.racion__intake');
		var daysFood = modalPopup.querySelectorAll('.modal__days-food');
		var numIntakes = 5;
		var numInGroup = slides.length / numIntakes;
		var j = 0;
		var jEnd = 5;

		for (var i = 0; i < numInGroup; i++) {
			// daysFood[i]

			for (; j < jEnd; j++) {
				slides[j].setAttribute('current-day', i + 1);
			}

			jEnd += 5;
		}

		for (var i = 0; i < daysFood.length; i++) {
			daysFood[i].dataset.currentDay = i + 1;
		}
	}

	if(mediaUpLGRacion) {
		var slides = modalRacion.querySelectorAll('.swiper-slide');
		var daysFood = modalPopup.querySelectorAll('.modal__days-food');

		for (var i = 0; i < slides.length; i++) {
				slides[i].setAttribute('current-day', i + 1);
		}

		for (var i = 0; i < daysFood.length; i++) {
				daysFood[i].dataset.currentDay = i + 1;
		}
	}
}

function checkBtnRemoveDay() {
	var modalPopup = document.getElementById('modal--menu-personal-js');

	if (modalPopup) {
		var headers = modalPopup.querySelectorAll('.modal__days-item-header');

		if (headers.length === 1) {

			var btn = headers[0].querySelector('.modal__food-dishes-item-btn--day');

			if (!btn.classList.contains('hidden')) {
				btn.classList.add('hidden');
			}
		} else if (headers.length > 1) {

						var weeks = modalPopup.querySelectorAll('.modal__weeks-item-header');

						if(weeks.length === 1) {
								for (var i = 0; i < headers.length; i++) {
										var btn = headers[i].querySelector('.modal__food-dishes-item-btn--day');

										if (btn.classList.contains('hidden')) {
												btn.classList.remove('hidden');
										}
								}
						} else {
								for (var i = 0; i < headers.length; i++) {
										var btn = headers[i].querySelector('.modal__food-dishes-item-btn--day');
										btn.classList.add('hidden');
								}

								headers = modalPopup.querySelector('.modal__days:last-child').querySelectorAll('.modal__days-item-header');

								for (var i = 0; i < headers.length; i++) {
										var btn = headers[i].querySelector('.modal__food-dishes-item-btn--day');

										if (btn.classList.contains('hidden')) {
												btn.classList.remove('hidden');
										}
								}
						}
		}
	}
}

function resetDayNum() {
	var modal = document.getElementById('racion-js');
	var modalPopup = document.getElementById('modal--menu-personal-js');

		var daysHeader = modalPopup.querySelectorAll('.modal__days-item-header');

	if (mediaDownLGRacion) {
		var slides = modal.querySelectorAll('.racion__intake');
		var numInGroup = slides.length / 5;

		for (var i = 1; i <= numInGroup; i++) {
			var groupIntakes = modal.querySelectorAll('[current-day=\"' + i + '\"]');
			var dayTitle = groupIntakes[0].querySelector('.racion__intake-day-title');
			dayTitle.textContent = 'День ' + i;
		}
	}

		if(mediaUpLGRacion) {
				var slides = modal.querySelectorAll('.swiper-slide');

				for (var i = 0; i < slides.length; i++) {
						var dayTitle = slides[i].querySelector('.racion__intake-day-title');
						dayTitle.textContent = 'День ' + (i + 1);
				}
		}

		for (var i = 0, j = 1; i < daysHeader.length; i++,j++) {
				var title = daysHeader[i].querySelector('.modal__days-item-header-key');

				if(j > 7) j = 1;

				title.textContent = 'День ' + j;
		}
}

function removeDayPopup(btn) {
		var racion = document.getElementById('racion-js');
		var modalPopup = document.getElementById('modal--menu-personal-js');
		var days = $(btn).parents('.modal__days');
		var daysHeader = days.find('.modal__days-item-header');
		var header = $(btn).parents('.modal__days-item-header');
		var listFood = header[0].nextElementSibling;
		var currentDay = listFood.dataset.currentDay;
		var headers = modalPopup.querySelectorAll('.modal__days-item-header');

		if (headers.length > 1) {
				header.remove();
				listFood.remove();

				if (mediaDownLGRacion) {
						var intakes = racion.querySelectorAll('.racion__intake[current-day=\"' + currentDay + '\"]');

						for (var i = 0; i < intakes.length; i++) {
								var slide = intakes[i].parentElement;
								racionSwiper.removeSlide($(slide).index());
								racionSwiper.update(true);
						}

						var addIntakeBtn = document.createElement('button');
						addIntakeBtn.className = 'racion__intake-add-btn';
						addIntakeBtn.type = 'button';
						addIntakeBtn.append('Cоздать новый день');
						addNewDay(addIntakeBtn);

						var remainingIntakes = racion.querySelectorAll('.racion__intake');
						var lastIntake = remainingIntakes[remainingIntakes.length - 1];
						var addBlockLastIntake = lastIntake.querySelector('.racion__intake-add-block');
						var btnAddBlockLastIntake = addBlockLastIntake.querySelector('.racion__intake-add-btn');

						if (!btnAddBlockLastIntake) {
								addBlockLastIntake.append(addIntakeBtn);
						}

						resetCurrentDayAttr();
						resetDayNum();
						racionSwiper.update(true);
				}

				if(mediaUpLGRacion) {
						var intakes = racion.querySelector('.swiper-slide[current-day=\"' + currentDay + '\"]'),
								swipeBtnAdd = intakes.querySelector('.racion__intake-add-btn');

						if(swipeBtnAdd) {
								var swipeBtnAddClone = swipeBtnAdd.cloneNode(true);
						}

						racionSwiper.removeSlide($(intakes).index());

						if(swipeBtnAdd) {
								var lastSlide = racion.querySelector('.swiper-wrapper').querySelector('.swiper-slide:last-child');
								lastSlide.querySelector('.racion__intake-add-block').append(swipeBtnAddClone);
								addNewDay(lastSlide);
						}

						calculateCostMenuPersonalByDay();

						resetCurrentDayAttr();
						resetDayNum();
						racionSwiper.update(true);
				}
		}

		if(daysHeader.length <= 1) {
				var buttonAdd = days[0].querySelector('#add-day-menu-personal-js');
				days.prevAll('.modal__days:first').append(buttonAdd);

				days.prevAll('.modal__weeks-item-header:first').remove();
				days.remove();

				//addNewDay(document.getElementById('add-day-menu-personal-js'));
		}

		//checkDaysInWeeks();

		checkBtnRemoveDay();
		checkBtnDoubleDay();

		checkBtnsDoubleWeek();
}

function removeDishesItemCarousel(currentDay, eatTime, indexElem, selectedItem) {
	var racion = document.getElementById('racion-js');

	if (mediaDownLGRacion) {
		var intakes = racion.querySelectorAll('.swiper-slide .racion__intake[current-day=\"' + currentDay + '\"]');

		var intake = racion.querySelector('.swiper-slide .racion__intake[current-day=\"' + currentDay + '\"][eat-time=\"' + eatTime + '\"]');
	} else if (mediaUpLGRacion) {
		var intakes = racion.querySelectorAll('.swiper-slide[current-day=\"' + currentDay + '\"]');
		var intake = intakes[0].querySelector('.racion__intake[eat-time=\"' + eatTime + '\"]');
	}

	var dayNorm = intakes[0].querySelector('.racion__intake-day-value');
	var totalCost = intakes[intakes.length - 1].querySelector('.racion__intake-add-price-value');
	var listDishes = intake.querySelector('.racion__dishes');
	var dishes = listDishes.querySelectorAll('.racion__dishes-item--not-empty');
	listDishes.removeChild(dishes[indexElem]);

	calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'remove', currentDay);
	calculateCostMenuPersonal(selectedItem.price, totalCost, 'remove');

	setTimeout(function() {
		racionSwiper.update(true);
	}, 100);
}

//e01
function showEatTimeModal() {
	var btnAdd = document.querySelectorAll('.menu-personal-block__btn');
	var modal = $('.modal--choice');

	for (var i = 0; i < btnAdd.length; i++) {

		btnAdd[i].onclick = function(event) {
			var thisBtn = $(this);
			var parentItem = this.parentElement.parentElement.parentElement;
			var idParentItem = parentItem.dataset.id;

			if (mediaUpLGRacion) {
				if (!checkActiveDishesSection) {
					toggleDropdownShow(thisBtn, 'on');
				} else {
					toggleDropdownShow(thisBtn, 'off');
				}
			}

			$.ajax({
				url: urlJson,
				dataType: 'json',
				data: {id:idParentItem},
				success: function(result) {
					var object = result[idParentItem];
					selectedItem.itemId = object.itemId;
					selectedItem.weight = object.weight;
					selectedItem.kkal = object.kkal;
					selectedItem.price = object.price;
					selectedItem.proteins = object.proteins;
					selectedItem.carbohydrates = object.carbohydrates;
					selectedItem.fat = object.fat;
					selectedItem.miniImg = object.miniImg;
										selectedItem.name = object.name;

					var dropdown = this.nextElementSibling;

					if (!checkActiveDishesSection) {

						if (mediaUpLGRacion) {

						} else if (mediaDownLGRacion) {
							modal.fadeIn(400);
						}
					} else {
						var activeDishesSection = document.querySelector('.racion__intake--active');

						if (mediaDownLGRacion) {
							var currentDayActiveSection = activeDishesSection.getAttribute('current-day');
						} else if (mediaUpLGRacion) {
							var currentDayActiveSection = activeDishesSection.parentElement.getAttribute('current-day');
						}

						var eatTimeActiveSection = activeDishesSection.getAttribute('eat-time');
						addDishesToModal(currentDayActiveSection, eatTimeActiveSection, selectedItem);

						if (activeDishesSection) {
							var emptyDishesItem = activeDishesSection.querySelector('.racion__dishes-item:not(.racion__dishes-item--not-empty)');
							emptyDishesItem.dataset.id = idParentItem;
							var containerEmptyDishesItem = emptyDishesItem.parentNode;
							var imgEmptyDishesItem = emptyDishesItem.querySelector('.racion__dishes-image');
							var nameplateEmptyDishesItem = emptyDishesItem.querySelector('.racion__dishes-nameplate');


							imgEmptyDishesItem.src = selectedItem.miniImg;
							nameplateEmptyDishesItem.textContent = selectedItem.kkal;
							doubleDishesItem(containerEmptyDishesItem);
							emptyDishesItem.classList.add('racion__dishes-item--not-empty');
							activeDishesSection.classList.remove('racion__intake--active');
							var dayNorm = '';

							if (mediaDownLGRacion) {
								var racionActive = document.querySelectorAll('[active-add-racion="true"]');

								dayNorm = racionActive[0].querySelector('.racion__intake-day-value');
								var totalCost = racionActive[racionActive.length - 1].querySelector('.racion__intake-add-price-value');
							} else if (mediaUpLGRacion) {
								activeDishesSection.previousElementSibling.classList.remove('racion__intake--next-active');
								var priceDishesItem = emptyDishesItem.querySelector('.racion__dishes-price');
								priceDishesItem.append(selectedItem.price + ' р.');

								var swiperSlideActive = document.querySelector('.racion .swiper-slide-active');
								dayNorm = swiperSlideActive.querySelector('.racion__intake-day-value');
								var totalCost = swiperSlideActive.querySelector('.racion__intake-add-price-value');
							}

							calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', currentDayActiveSection);
							calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
							checkActiveDishesSection = false;
						}
					}
				}
			});
		};
	}
}

function doubleDishesItem(dishes) {

	var dishesItem = document.createElement('div');
	dishesItem.className = 'racion__dishes-item';

	var dishesItemContent = document.createElement('div');
	dishesItemContent.className = 'racion__dishes-item-content';

	var dishesBtnDel = document.createElement('button');
	dishesBtnDel.className = 'racion__dishes-btn-del';
	dishesBtnDel.type = 'button';
	removeDishesItem(dishesBtnDel);

	var dishesImage = document.createElement('img');
	dishesImage.className = 'racion__dishes-image';

	var dishesNameplate = document.createElement('span');
	dishesNameplate.className = 'racion__dishes-nameplate';

	dishesItemContent.append(dishesBtnDel, dishesImage, dishesNameplate);

	if (mediaUpLGRacion) {
		var dishesPrice = document.createElement('span');
		dishesPrice.className = 'racion__dishes-price';

		dishesItem.append(dishesItemContent, dishesPrice);
	} else if (mediaDownLGRacion) {
		dishesItem.append(dishesItemContent);
	}

	clickDishesFromCarousel(dishesItem);
	dishes.append(dishesItem);

	if (mediaDownLGRacion) {
		setTimeout(function() {
			racionSwiper.update(true);
		}, 100);
	}
}

function moveToSlideRacion(slide, slider) {
	var index = 0;

	while( (slide = slide.previousSibling) != null ) {
		index++;
	}

	slider.slideTo(index);
}

function selectModalEatTime() {
		var modal = $('.modal--choice');
		var modalBullet = document.querySelectorAll('.modal__choice-item');

		$(document).on('click','.modal__choice-item',function(){
				var activeIntakes = document.querySelectorAll('.racion__intake[active-add-racion="true"]');
				var modalEatTime = $(this).attr('eat-time');

				for (var i = 0; i < activeIntakes.length; i++) {
						var eatTime = activeIntakes[i].getAttribute('eat-time');

						if (eatTime === modalEatTime) {
								var intakeDishesContainer = activeIntakes[i].querySelector('.racion__dishes');
								var intakeDishes = activeIntakes[i].querySelector('.racion__dishes-item:not(.racion__dishes-item--not-empty)');
								intakeDishes.dataset.id = selectedItem.itemId;

								intakeDishes.classList.add('racion__dishes-item--not-empty');
								var imgIntakeDishes = intakeDishes.querySelector('.racion__dishes-image');
								var nameplateIntakeDishes = intakeDishes.querySelector('.racion__dishes-nameplate');
								imgIntakeDishes.src = selectedItem.miniImg;
								nameplateIntakeDishes.textContent = selectedItem.kkal;
								doubleDishesItem(intakeDishesContainer);


								var swiperSlide = intakeDishesContainer.parentNode.parentNode;

								if (mediaDownLGRacion) {
										moveToSlideRacion(swiperSlide, racionSwiper);

										var racionActive = document.querySelectorAll('[active-add-racion="true"]');
										var dayNorm = racionActive[0].querySelector('.racion__intake-day-value');
										var currentDay = racionActive[0].getAttribute('current-day');
										var totalCost = racionActive[racionActive.length - 1].querySelector('.racion__intake-add-price-value');
										calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', currentDay);
										calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
								}
						}
				}

				var currentDay = activeIntakes[0].getAttribute('current-day');
				addDishesToModal(currentDay, modalEatTime, selectedItem);

				modal.fadeOut(400);
		})
}

function dragMenuPersonal() {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		$('.menu-personal-block__item').draggable({
			helper: function() {
				var helperWrapper = document.createElement('div');
				helperWrapper.className = 'menu-personal-block__item-helper';

				var helperImage = document.createElement('img');
				helperImage.className = 'menu-personal-block__helper-image';

				helperWrapper.append(helperImage);
				return helperWrapper;
			},
			cursorAt: {
				bottom: 15,
				left: 30
			},
			start: function(event, ui) {
				var activeIntake = document.querySelector('.racion__intake.racion__intake--active');
				var beforeActiveIntake = document.querySelector('.racion__intake--next-active');

				if (activeIntake) {
					activeIntake.classList.remove('racion__intake--active');
				}

				if (beforeActiveIntake) {
					beforeActiveIntake.classList.remove('racion__intake--next-active');
				}

				$('.menu-personal-block__item-bottom.show .menu-personal-block__btn').dropdown('toggle');



				var item = event.target;
				var itemId = item.dataset.id;

				var helper = ui.helper[0];

				$.ajax({
					url: urlJson,
					dataType: 'json',
					data: {id:itemId},
					success: function(result) {
						var object = result[itemId];
						selectedItem.weight = object.weight;
						selectedItem.kkal = object.kkal;
						selectedItem.price = object.price;
						selectedItem.proteins = object.proteins;
						selectedItem.fat = object.fat;
						selectedItem.carbohydrates = object.carbohydrates;
						selectedItem.miniImg = object.miniImg;
						selectedItem.name = object.name;

						var helperImage = helper.firstElementChild;
						helperImage.src = selectedItem.miniImg;
						checkActiveDishesSection = false;
					}
				});
			}
		});
	}
}

function dropMenuPersonal(intake) {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		$(intake).droppable({
			over: function(event, ui) {
				setTimeout(function() {
					removeIntakeActiveClass();
					var intakeElement = event.target;
					var prevIntakeElement = intake.previousElementSibling;

					if (!intakeElement.classList.contains('racion__intake--active')) {
						intakeElement.classList.add('racion__intake--active');
						prevIntakeElement.classList.add('racion__intake--next-active');
						checkActiveDishesSection = true;
					}
				}, 10);
			},
			drop: function(event, ui) {
				var intakeDrop = event.target;
				var dropEatTime = intakeDrop.getAttribute('eat-time');
				var dishesBlock = intakeDrop.lastElementChild;
				var lastDishesItem = dishesBlock.lastElementChild;
				var contentDishesItem = lastDishesItem.firstElementChild;
				var priceDishesItem = lastDishesItem.lastElementChild;
				var nameplateDishesItem = contentDishesItem.lastElementChild;
				var imgDishesItem = nameplateDishesItem.previousElementSibling;

				var draggableItem = ui.draggable[0];

				var idDraggableItem = draggableItem.dataset.id;

				$.ajax({
					url: urlJson,
					dataType: 'json',
					data: {id:idDraggableItem},
					success: function(result) {
						var object = result[idDraggableItem];
						selectedItem.itemId = object.itemId;
						selectedItem.weight = object.weight;
						selectedItem.kkal = object.kkal;
						selectedItem.price = object.price;
						selectedItem.proteins = object.proteins;
						selectedItem.fat = object.fat;
						selectedItem.carbohydrates = object.carbohydrates;
						selectedItem.miniImg = object.miniImg;
						selectedItem.name = object.name;

						if (lastDishesItem) {
							lastDishesItem.classList.add('racion__dishes-item--not-empty');
							lastDishesItem.dataset.id = selectedItem.itemId;
							nameplateDishesItem.append(selectedItem.kkal);
							imgDishesItem.src = selectedItem.miniImg;
							priceDishesItem.append(selectedItem.price + ' р.');
							doubleDishesItem(dishesBlock);
							removeIntakeActiveClass();
							checkActiveDishesSection = false;

							var activeSlide = document.querySelector('.racion .swiper-slide-active');
							var numCurrentDay = activeSlide.getAttribute('current-day');
							var dayNorm = activeSlide.querySelector('.racion__intake-day-value');
							var totalCost = activeSlide.querySelector('.racion__intake-add-price-value');
							calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', numCurrentDay);
							calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
							addDishesToModal(numCurrentDay, dropEatTime, selectedItem);
							calculateRacionPopupBgHeight();
						}
					}
				});
			},
			out: function(event, ui) {
				removeIntakeActiveClass();
				checkActiveDishesSection = false;
			}
		});
	}
}

function disableDroppableAreas() {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		var notActiveSlides = document.querySelectorAll('.racion .swiper-slide:not(.swiper-slide-active)');

		if (notActiveSlides.length > 0) {

			for (var i = 0; i < notActiveSlides.length; i++) {
				var intakes = notActiveSlides[i].querySelectorAll('.racion__intake');

				for (var j = 0; j < intakes.length; j++) {
					$(intakes[j]).droppable('disable');
				}
			}
		}
	}
}

function enableDroppableAreas() {

	if (window.matchMedia("screen and (min-width: 1200px)").matches) {
		var activeSlide = document.querySelector('.racion .swiper-slide-active');
		var intakes = activeSlide.querySelectorAll('.racion__intake');

		for (var i = 0; i < intakes.length; i++) {
			$(intakes[i]).droppable('enable');
		}
	}
}

function createIntakesDropdowns(numIntakes, namesIntakes) {

	var items = document.querySelectorAll('.menu-personal-block__item');

	for (var i = 0; i < items.length; i++) {
		var itemWrapper = items[i].firstElementChild;
		var itemBottomContent = itemWrapper.lastElementChild;
		var itemBtn = itemBottomContent.firstElementChild;
		var afterBtnElement = itemBtn.nextElementSibling;


		var dropdown = document.createElement('div');
		dropdown.className = 'dropdown-menu';

		for (var j = 0; j < numIntakes; j++) {
			var dropdownBtn = document.createElement('button');
			dropdownBtn.className = 'dropdown-item';
			dropdownBtn.type = 'button';
			dropdownBtn.setAttribute('eat-time', namesIntakes[j]);

			var dropdownBtnSpan = document.createElement('span');
			dropdownBtnSpan.append(namesIntakes[j]);

			dropdownBtn.append(dropdownBtnSpan);
			choiceEatTimeDesktop(dropdownBtn);

			dropdown.append(dropdownBtn);
		}

		// itemBottomContent.insertBefore(dropdown, afterBtnElement);

		if(!itemBtn.nextElementSibling.classList.contains('dropdown-menu')) {
			itemBottomContent.insertBefore(dropdown, afterBtnElement);
		}
	}
}

function choiceEatTimeDesktop(btn) {

	$(btn).click(function(event) {

		var currentDay = $('.racion .swiper-slide-active');
		var numCurrentDay = currentDay[0].getAttribute('current-day');
		var catalogItem = $(this).parents('.menu-personal-block__item');
		var idParentItem = catalogItem.data('id');


		var nameplateCatalogItem = catalogItem.data('nameplate');
		var imageCatalogItem = catalogItem.data('miniImg');
		var priceCatalogItem = catalogItem.data('price');

		var btnEatTime = $(this).attr('eat-time');
		var intakeCurrentDay = currentDay.find('.racion__intake[eat-time=\"' + btnEatTime + '\"]');

		var dishes = intakeCurrentDay.find('.racion__dishes');
		var dishesItem = intakeCurrentDay.find('.racion__dishes-item:not(".racion__dishes-item--not-empty")');
		var imgDishesItem = dishesItem.find('.racion__dishes-image');
		var nameplateDishesItem = dishesItem.find('.racion__dishes-nameplate');
		var priceDishesItem = dishesItem.find('.racion__dishes-price');



		$.ajax({
			url: urlJson,
			dataType: 'json',
			data: {id:idParentItem},
			success: function(result) {
				var object = result[idParentItem];
				selectedItem.itemId = object.itemId;
				selectedItem.weight = object.weight;
				selectedItem.kkal = object.kkal;
				selectedItem.price = object.price;
				selectedItem.proteins = object.proteins;
				selectedItem.fat = object.fat;
				selectedItem.carbohydrates = object.carbohydrates;
				selectedItem.miniImg = object.miniImg;
				selectedItem.name = object.name;

				dishesItem[0].dataset.id = selectedItem.itemId;
				imgDishesItem.prop('src', selectedItem.miniImg);
				nameplateDishesItem.append(selectedItem.kkal);
				priceDishesItem.append(selectedItem.price + ' р.');

				doubleDishesItem(dishes[0]);

				dishesItem.addClass('racion__dishes-item--not-empty');

				var dayNorm = currentDay[0].querySelector('.racion__intake-day-value');
				var totalCost = currentDay[0].querySelector('.racion__intake-add-price-value');
				calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', numCurrentDay);
				calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
				addDishesToModal(numCurrentDay, btnEatTime, selectedItem);
				calculateRacionPopupBgHeight();
			}
		});
	});
}

function calculateRacionPopupBgHeight() {

	if (mediaUpLGRacion) {
		var modalBlock = $('.modal--menu-personal .modal__block');
		var modalHeight = modalBlock.outerHeight();
		var res = modalCreateRacionBgHeight + 17 - modalHeight;

		if (res < 0) {
			modalCreateRacionBg.css('bottom', res + 'px');
		} else if (res >= 0) {
			modalCreateRacionBg.css('bottom', 0 + 'px');
		}
	}
}

function createIntakesModal() {
	var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');

	if (inputRacion) {
		var numIntakes = inputRacion.value;
		var namesIntakes = inputRacion.dataset.intakeNames;
		namesIntakes = namesIntakes.split(', ');

		if (mediaDownLGRacion) {
			var modal = document.getElementById('modal__choice-js');
			var fragment = document.createDocumentFragment();


			for (var i = 0; i < numIntakes; i++) {
				var modalBullet = document.createElement('li');
				modalBullet.className = 'modal__choice-item';
				modalBullet.setAttribute('eat-time', namesIntakes[i]);
				modalBullet.append(namesIntakes[i]);
				fragment.appendChild(modalBullet);
			}

						modal.innerHTML = '';

			modal.appendChild(fragment);
		} else if (mediaUpLGRacion) {
			createIntakesDropdowns(numIntakes, namesIntakes);

			/*$('.menu-personal-block__btn').dropdown({
				flip: false
			});*/

			createIntakesModalDropdown(numIntakes, namesIntakes);

			/*$('.modal__add-racion-js').dropdown({
				flip: false
			});*/

			hideIntakesItemDropdown(inputRacion);
		}
	}
}

function doubleIntakes(dayNumber, doubleDay) {

		if(mediaUpLGRacion) {
				var slider = document.querySelector('.swiper-wrapper'),
						slide = slider.querySelector('.swiper-slide[current-day=\"' + doubleDay + '\"]'),
						slideClone = slide.cloneNode(true),
						slides,
						dishesBtnDel = slideClone.querySelectorAll('.racion__dishes-btn-del'),
						intakeBtnDel = slideClone.querySelectorAll('.racion__intake-addition-btn'),
						dishesItem = slideClone.querySelectorAll('.racion__dishes-item'),
						intake = slideClone.querySelectorAll('.racion__intake'),
						btnAddDay = slideClone.querySelector('.racion__intake-add-btn');

				$(slide).after(slideClone);

				slides = slider.querySelectorAll('.swiper-slide');

				for(var i = 0; i < slides.length; i++) {
						slides[i].setAttribute('current-day',(i + 1));
						slides[i].querySelector('.racion__intake-day-title').textContent = "День " + (i + 1);
				}

				for(var i = 0; i < dishesBtnDel.length; i++) {
						removeDishesItem(dishesBtnDel[i]);
				}

				for(var i = 0; i < intakeBtnDel.length; i++) {
						сancellationDishes(intakeBtnDel[i]);
				}

				for(var i = 0; i < dishesItem.length; i++) {
						clickDishesFromCarousel(dishesItem[i]);
				}

				for(var i = 0; i < intake.length; i++) {
						dropMenuPersonal(intake[i]);
				}

				doubleIntakeBlockLI();
	addNewDay(document.querySelector('.racion__intake-add-btn'));

				recalculateRacionSwiper();

				var prevBtnSlider = $('.racion .swiper-button-prev');
				var nextBtnSlider = $('.racion .swiper-button-next');

				prevBtnSlider.addClass('swiper-button--visible');
				nextBtnSlider.addClass('swiper-button--visible');
		}

		if(mediaDownLGRacion) {
				var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
	var numIntakes = inputRacion.value;

				var slider = document.querySelector('.swiper-wrapper'),
						slides = slider.querySelectorAll('.racion__intake[current-day=\"' + doubleDay + '\"]'),
						lastSlide = slides[slides.length - 1].parentElement,
						allSlides;

				for(var i = 0; i < slides.length; i++) {
						var clone = slides[i].parentElement.cloneNode(true),
								dishesBtnDel = clone.querySelector('.racion__dishes-btn-del'),
								dishesItem = clone.querySelector('.racion__dishes-item');

						$(lastSlide).after(clone);
						lastSlide = clone;

						removeDishesItem(dishesBtnDel);
						clickDishesFromCarousel(dishesItem);
				}

				allSlides = slider.querySelectorAll('.swiper-slide');

				for(var i = 0, j = 0; i < allSlides.length; i++) {
						var intake = allSlides[i].querySelector('.racion__intake');

						if(i % numIntakes == 0) {
								j++;

								intake.querySelector('.racion__intake-day-title').textContent = "День " + j;
						}

						intake.setAttribute('current-day', j);
				}

				doubleIntakeBlockLI();
				addNewDay(document.querySelector('.racion__intake-add-btn'));
		}

		racionSwiper.update(true);
}

function doubleIntakeBlockLI() {
		var carousel = document.querySelector('.racion .swiper-container');
		var carouselWrapper= carousel.querySelector('.swiper-wrapper');
		var swiperSlides = carouselWrapper.querySelectorAll('.swiper-slide');

		for (var i = 0; i < swiperSlides.length; i++) {
				var existAddIntakeBlock = swiperSlides[i].querySelector('.racion__intake-add-block');

				if (existAddIntakeBlock) {

						if (i !== swiperSlides.length -1) {
								var btn = existAddIntakeBlock.querySelector('.racion__intake-add-btn');

								if (btn) {
										existAddIntakeBlock.removeChild(btn);
								}
						}
				}
		}
}

function hideModalBullet(checkedInput) {

	if (checkedInput) {
		var modal = document.getElementById('modal__choice-js');

		var trueIntakes = checkedInput.dataset.trueIntakes;
		var hiddenIntakes = checkedInput.dataset.hiddenIntakes;
		hiddenIntakes = hiddenIntakes.split(', ');
		var allBullets = modal.querySelectorAll('.modal__choice-item');

		if (trueIntakes === '3') {

			for (var i = 0; i < allBullets.length; i++) {

				var eatTime = allBullets[i].getAttribute('eat-time');

				for (var j = 0; j < hiddenIntakes.length; j++) {

					if (eatTime === hiddenIntakes[j]) {

						if (!allBullets[i].classList.contains('hidden')) {
							allBullets[i].classList.add('hidden');
						}
					}
				}
			}
		} else if (trueIntakes === '5') {

			for (var i = 0; i < allBullets.length; i++) {

				if (allBullets[i].classList.contains('hidden')) {
					allBullets[i].classList.remove('hidden');
				}
			}
		}
	}
}

function hideIntakesItemDropdown(checkedInput) {

	if (checkedInput) {

		if (mediaUpLGRacion) {
			var trueIntakes = checkedInput.dataset.trueIntakes;
			var hiddenIntakes = checkedInput.dataset.hiddenIntakes;
			hiddenIntakes = hiddenIntakes.split(', ');
			var items = document.querySelectorAll('.menu-personal-block__item');

			if (trueIntakes === '3') {
				for (var i = 0; i < items.length; i++) {
					var item = items[i];
					var buttons = item.querySelectorAll('.dropdown-item');
					for (var j = 0; j < buttons.length; j++) {
						var eatTime = buttons[j].getAttribute('eat-time');
						for (var n = 0; n < hiddenIntakes.length; n++) {

							if (eatTime === hiddenIntakes[n]) {

								if (!buttons[j].classList.contains('hidden')) {
									buttons[j].classList.add('hidden');
								}
							}
						}
					}
				}

				//modal
				var modal = document.querySelector('.modal--menu-personal-block');
				var buttons = modal.querySelectorAll('.dropdown-item');

				for (var i = 0; i < buttons.length; i++) {
					var eatTime = buttons[i].getAttribute('eat-time');

					for (var j = 0; j < hiddenIntakes.length; j++) {

						if (eatTime === hiddenIntakes[j]) {

							if (!buttons[i].classList.contains('hidden')) {
								buttons[i].classList.add('hidden');
							}
						}
					}
				}
			} else if (trueIntakes === '5') {
				for (var i = 0; i < items.length; i++) {
					var buttons = items[i].querySelectorAll('.dropdown-item');

					for (var j = 0; j < buttons.length; j++) {

						if (buttons[j].classList.contains('hidden')) {
							buttons[j].classList.remove('hidden');
						}
					}
				}

				var modal = document.querySelector('.modal--menu-personal-block');
				var buttons = modal.querySelectorAll('.dropdown-item');

				for (var i = 0; i < buttons.length; i++) {
					var eatTime = buttons[i].getAttribute('eat-time');

					for (var j = 0; j < hiddenIntakes.length; j++) {

						if (eatTime === hiddenIntakes[j]) {

							if (buttons[i].classList.contains('hidden')) {
								buttons[i].classList.remove('hidden');
							}
						}
					}
				}
			}
		} else if (mediaDownLGRacion) {
			var trueIntakes = checkedInput.dataset.trueIntakes;
			var hiddenIntakes = checkedInput.dataset.hiddenIntakes;
			hiddenIntakes = hiddenIntakes.split(', ');

			var modal = document.getElementById('modal__choice-js');
			var items = modal.querySelectorAll('.modal__choice-item');

			if (trueIntakes === '3') {
				for (var i = 0; i < items.length; i++) {
					var eatTime = items[i].getAttribute('eat-time');

					for (var n = 0; n < hiddenIntakes.length; n++) {
						if (eatTime === hiddenIntakes[n]) {

							if (!items[i].classList.contains('hidden')) {
											items[i].classList.add('hidden');
							}
						}
					}
				}
			} else if (trueIntakes === '5') {
				for (var i = 0; i < items.length; i++) {
					if (items[i].classList.contains('hidden')) {
						items[i].classList.remove('hidden');
					}
				}
			}
		}
	}
}

function hideIntakes(checkedInput) {

	if (checkedInput) {
		var carousel = document.querySelector('.racion .swiper-container');

		var trueIntakes = checkedInput.dataset.trueIntakes;
		var hiddenIntakes = checkedInput.dataset.hiddenIntakes;
		hiddenIntakes = hiddenIntakes.split(', ');
		var allIntakes = carousel.querySelectorAll('.racion__intake');

		if (trueIntakes === '3') {

			for (var i = 0; i < allIntakes.length; i++) {
				var eatTime = allIntakes[i].getAttribute('eat-time');

				for (var j = 0; j < hiddenIntakes.length; j++) {

					if (hiddenIntakes[j] === eatTime) {

						if (mediaDownLGRacion) {
							var hideSlide = allIntakes[i].parentElement;
						} else if (mediaUpLGRacion) {
							hideSlide = allIntakes[i];
						}

						if (!hideSlide.classList.contains('slide-invisible')) {
							hideSlide.classList.add('slide-invisible');
						}
					}
				}
			}
		} else if (trueIntakes === '5') {
			for (var i = 0; i < allIntakes.length; i++) {

				if (mediaDownLGRacion) {
					var hideSlide = allIntakes[i].parentElement;
				} else if (mediaUpLGRacion) {
					hideSlide = allIntakes[i];
				}

				if (hideSlide.classList.contains('slide-invisible')) {
					hideSlide.classList.remove('slide-invisible');
				}
			}
		}
	}
}

function calculateCreateRacionHeight() {

	if (mediaUpLGRacion) {
		var modalBlock = $('.modal--menu-personal .modal__block');
		var modalHeight = modalBlock.outerHeight();
		var res = modalCreateRacionBgHeight + 17 - modalHeight;

		if (res < 0) {
			modalCreateRacionBg.css('bottom', res + 'px');
		} else if (res >= 0) {
			modalCreateRacionBg.css('bottom', 0 + 'px');
		}
	}
}

function hideModalsFoodItem(checkedInput) {

	if (checkedInput) {
		var trueIntakes = checkedInput.dataset.trueIntakes;
		var hiddenIntakes = checkedInput.dataset.hiddenIntakes;
		hiddenIntakes = hiddenIntakes.split(', ');

		var modal = document.getElementById('modal--menu-personal-js');

		if (modal) {
			var foodItems = modal.querySelectorAll('.modal__days-food-item');

			if (trueIntakes === '3') {

				for (var i = 0; i < foodItems.length; i++) {
					var eatTime = foodItems[i].getAttribute('eat-time');

					for (var j = 0; j < hiddenIntakes.length; j++) {

						if (eatTime === hiddenIntakes[j]) {

							if (!foodItems[i].classList.contains('hidden')) {
								foodItems[i].classList.add('hidden');
							}
						}
					}
				}
			} else if (trueIntakes === '5') {

				for (var i = 0; i < foodItems.length; i++) {

					if (foodItems[i].classList.contains('hidden')) {
						foodItems[i].classList.remove('hidden');
					}
				}
			}

			calculateCreateRacionHeight();
		}
	}
}

function createIntakes(dayNumber) {
	var carousel = document.querySelector('.racion .swiper-container');
	var inputRacion = document.querySelector('.modal--create-racion .field-radio__name--personal input:checked');
	var numIntakes = inputRacion.value;
	var namesIntakes = inputRacion.dataset.intakeNames;
	var racionNorm = document.querySelector('.racion[data-norm]');
	var racionNormValue = normKkalValue;
	// var racionNormPrefix = racionNorm.dataset.normPrefix;
	namesIntakes = namesIntakes.split(', ');

	var media = '';

	if (mediaDownLGRacion) {
		media = 'mobile';
	} else if (mediaUpLGRacion) {
		media = 'desktop';
	}

	if (media === 'desktop') {
		constructorIntake(racionNormValue, dayNumber, media, namesIntakes, null, numIntakes);
	} else if (media === 'mobile') {

		for (var i = 0; i < numIntakes; i++) {

			if (i === 0) {
				var firstIntake = true;
				constructorIntake(racionNormValue, dayNumber, media, namesIntakes[i], firstIntake);
			} else {
				var firstIntake = false;
				constructorIntake(racionNormValue, dayNumber, media, namesIntakes[i], firstIntake);
			}
		}
	}


	addActiveRacionAttr(media, numIntakes, dayNumber);
	addDayNumToIntakes(media, dayNumber);
	addIntakeBlockLI();
	addNewDay(document.querySelector('.racion__intake-add-btn'));
	hideIntakes(inputRacion);
	// hideIntakesItemDropdown(inputRacion);
	racionSwiper.update(true);


	if (mediaUpLGRacion) {
		racionSwiper.slideNext();
	}
}

function addDayNumToIntakes(media, dayNumber) {
	var carousel = document.querySelector('.racion .swiper-container');

	if (media === 'mobile') {
		var existIntakes = carousel.querySelectorAll('.racion__intake');

		if (dayNumber === 1) {

			if (existIntakes.length > 0) {

				for (var i = 0; i < existIntakes.length; i++) {
					existIntakes[i].setAttribute('current-day', dayNumber);
				}
			}
		} else if (dayNumber > 1) {
			var existIntakesWithDay = carousel.querySelectorAll('.racion__intake[current-day]');

			if (existIntakesWithDay. length > 0) {
				var lastExistIntakesWithDay = existIntakesWithDay[existIntakesWithDay.length - 1];
				var valueLastExistDay = lastExistIntakesWithDay.getAttribute('current-day');
				var existIntakesWithOutDay = carousel.querySelectorAll('.racion__intake:not([current-day])');

				for (var i = 0; i < existIntakesWithOutDay.length; i++) {
					existIntakesWithOutDay[i].setAttribute('current-day', +valueLastExistDay + 1);
				}
			}
		}
	} else if (media === 'desctop') {

	}
}

function addActiveRacionAttr(media, numIntakes, dayNumber) {
	var carousel = document.querySelector('.racion .swiper-container');

	if (media === 'mobile') {
		var allIntakes = carousel.querySelectorAll('.racion__intake');

		if (dayNumber === 1) {

			for (var i = 0; i < allIntakes.length; i++) {
				allIntakes[i].setAttribute('active-add-racion', true);
			}
		} else if (dayNumber > 1) {

			if (dayNumber >= 3) {
				numIntakes = numIntakes * (dayNumber - 1);
			}

			for (var i = 0; i < allIntakes.length; i++) {
				allIntakes[i].setAttribute('active-add-racion', false);
			}

			for (var i = allIntakes.length - 1; i >= numIntakes; i--) {
				allIntakes[i].setAttribute('active-add-racion', true);
			}
		}
	}
}

function constructorIntake(racionNorm, dayNumber, media, nameIntake, firstIntake, numIntakes) {
	var swiperSlide = document.createElement('div');
	swiperSlide.className = 'swiper-slide';

	var intakeDay = document.createElement('div');
	intakeDay.className = 'racion__intake-day';

	var intake = document.createElement('div');
	intake.className = 'racion__intake';
	intake.setAttribute('eat-time', nameIntake);

	var intakeDayTitle = document.createElement('span');
	intakeDayTitle.className = 'racion__intake-day-title';
	intakeDayTitle.append('День ', dayNumber);

	var timeOfDay = document.createElement('span');
	timeOfDay.className = 'racion__time-of-day';
	timeOfDay.append(nameIntake);

	var intakeDayNorm = document.createElement('div');
	intakeDayNorm.className = 'racion__intake-day-norm';

	var intakeDayValue = document.createElement('span');
	intakeDayValue.className = 'racion__intake-day-value';
	intakeDayValue.append(racionNorm);

	var intakeDayMeasure = document.createElement('span');
	intakeDayMeasure.className = 'racion__intake-day-measure';
	intakeDayMeasure.append(' kkal');

	var dishes = document.createElement('div');
	dishes.className = 'racion__dishes';

	var dishesItem = document.createElement('div');
	dishesItem.className = 'racion__dishes-item';

	var dishesItemContent = document.createElement('div');
	dishesItemContent.className = 'racion__dishes-item-content';

	var dishesBtnDel = document.createElement('button');
	dishesBtnDel.className = 'racion__dishes-btn-del';
	dishesBtnDel.type = 'button';
	removeDishesItem(dishesBtnDel);

	var dishesImage = document.createElement('img');
	dishesImage.className = 'racion__dishes-image';

	var dishesNameplate = document.createElement('span');
	dishesNameplate.className = 'racion__dishes-nameplate';

	intakeDayNorm.append(intakeDayValue, intakeDayMeasure);
	dishesItemContent.append(dishesBtnDel, dishesImage, dishesNameplate);
	dishesItem.append(dishesItemContent);
	clickDishesFromCarousel(dishesItem);
	dishes.append(dishesItem);

	if (media === 'desktop') {
		var intakeDayAlert = document.createElement('span');
		intakeDayAlert.className = 'racion__intake-day-alert';
		intakeDayAlert.append('Вы превысили норму удалите лишнее');

		intakeDay.append(intakeDayTitle, intakeDayAlert, intakeDayNorm);
		swiperSlide.append(intakeDay);

		for (var i = 0; i < numIntakes; i++) {

			var intakeAddBlock = document.createElement('div');
			intakeAddBlock.className = 'racion__intake-addition';

			var intakeAddBlockText = document.createElement('span');
			intakeAddBlockText.className = 'racion__intake-addition-text';
			intakeAddBlockText.append('Добавьте блюдо');

			var intakeAddBlockBtn = document.createElement('button');
			intakeAddBlockBtn.className = 'racion__intake-addition-btn';
			intakeAddBlockBtn.type = 'button';
			intakeAddBlockBtn.setAttribute('aria-label', 'Отмена добавления');
			intakeAddBlockBtn.append(document.createElement('span'));
			сancellationDishes(intakeAddBlockBtn);

			intakeAddBlock.append(intakeAddBlockText, intakeAddBlockBtn);

			var intake = document.createElement('div');
			intake.className = 'racion__intake';
			intake.setAttribute('eat-time', nameIntake[i]);

			var timeOfDay = document.createElement('span');
			timeOfDay.className = 'racion__time-of-day';
			timeOfDay.append(nameIntake[i]);

			var dishes = document.createElement('div');
			dishes.className = 'racion__dishes';

			var dishesPrice = document.createElement('span');
			dishesPrice.className = 'racion__dishes-price';

			var dishesItem = document.createElement('div');
			dishesItem.className = 'racion__dishes-item';

			var dishesItemContent = document.createElement('div');
			dishesItemContent.className = 'racion__dishes-item-content';

			var dishesBtnDel = document.createElement('button');
			dishesBtnDel.className = 'racion__dishes-btn-del';
			dishesBtnDel.type = 'button';
			removeDishesItem(dishesBtnDel);

			var dishesImage = document.createElement('img');
			dishesImage.className = 'racion__dishes-image';

			var dishesNameplate = document.createElement('span');
			dishesNameplate.className = 'racion__dishes-nameplate';

			dishesItemContent.append(dishesBtnDel, dishesImage, dishesNameplate);
			dishesItem.append(dishesItemContent, dishesPrice);
			clickDishesFromCarousel(dishesItem);
			dishes.append(dishesItem);

			intake.append(intakeAddBlock, timeOfDay, dishes);
			dropMenuPersonal(intake);
			swiperSlide.append(intake);
		}
	} else if (media === 'mobile') {
		intakeDay.append(intakeDayTitle, intakeDayNorm);

		if (firstIntake) {
			intake.append(intakeDay, timeOfDay, dishes);
		} else {
			intake.append(timeOfDay, dishes);
		}
		swiperSlide.append(intake);
	}


	if (media === 'desktop') {
		recalculateRacionSwiper();
		swiperSlide.setAttribute('current-day', dayNumber);
		if (dayNumber > 1) {
			var prevBtnSlider = $('.racion .swiper-button-prev');
			var nextBtnSlider = $('.racion .swiper-button-next');

			prevBtnSlider.addClass('swiper-button--visible');
			nextBtnSlider.addClass('swiper-button--visible');
		}
	}

	racionSwiper.appendSlide(swiperSlide);
}

function recalculateRacionSwiper() {
	var animationDuration = 400;

	setTimeout(function() {
		var firstBlock = $('.categories');
		var secondBlock = $('.menu-personal-block__main');
		var thirdBlock = $('.racion');
		var bodyWidth = $('body').prop('clientWidth');
		var firstBlockWidth = firstBlock.outerWidth();
		var secondBlockWidth = secondBlock.outerWidth();
		var margins = parseInt(firstBlock.css('margin-right'), 10) + parseInt(thirdBlock.css('margin-left'), 10);
		var leftPaddingRacion = 0;

		var resWidth = bodyWidth - firstBlockWidth - secondBlockWidth - margins;

		if (mediaUpLGRacion) {
			leftPaddingRacion = 35
			resWidth = resWidth - leftPaddingRacion;
			$('.racion .swiper-container').css('width', resWidth + 'px');
			$('.racion .swiper-slide').css('width', resWidth + 'px');
		}

	}, animationDuration);
}

function addIntakeBlockLI() {
	var carousel = document.querySelector('.racion .swiper-container');
	var carouselWrapper= carousel.querySelector('.swiper-wrapper');
	var swiperSlides = carouselWrapper.querySelectorAll('.swiper-slide');

	for (var i = 0; i < swiperSlides.length; i++) {
		var existAddIntakeBlock = swiperSlides[i].querySelector('.racion__intake-add-block');

		if (existAddIntakeBlock) {

			if (i !== swiperSlides.length -1) {
				var btn = existAddIntakeBlock.querySelector('.racion__intake-add-btn');

				if (btn) {
					existAddIntakeBlock.removeChild(btn);
				}
			}
		}
	}

	// var lastSwiperSlide = swiperSlides[swiperSlides.length - 1];
	// var existAddIntakeBlock = lastSwiperSlide.querySelector('.racion__intake-add-block');

	if (existAddIntakeBlock) {
		var addBtn = existAddIntakeBlock.querySelector('.racion__intake-add-btn');
		//existAddIntakeBlock.removeElement(addBtn);
		existAddIntakeBlock.removeChild(addBtn);
	}

	var addIntakeBlock = document.createElement('div');
	addIntakeBlock.className = 'racion__intake-add-block';

	var addIntakePrice = document.createElement('div');
	addIntakePrice.className = 'racion__intake-add-price';

	var addIntakePriceValue = document.createElement('span');
	addIntakePriceValue.className = 'racion__intake-add-price-value';
	addIntakePriceValue.append('0');

	var addIntakePriceMeasure = document.createElement('span');
	addIntakePriceMeasure.className = 'racion__intake-add-price-measure';
	addIntakePriceMeasure.append(' руб.');

	var addIntakeBtn = document.createElement('button');
	addIntakeBtn.className = 'racion__intake-add-btn';
	addIntakeBtn.type = 'button';
	addIntakeBtn.append('Cоздать новый день');


	addIntakePrice.append(addIntakePriceValue, addIntakePriceMeasure);
	addIntakeBlock.append(addIntakePrice, addIntakeBtn);

	if (mediaUpLGRacion) {
		carouselWrapper.lastChild.append(addIntakeBlock);
	} else if (mediaDownLGRacion) {
		carouselWrapper.lastChild.children[0].append(addIntakeBlock);
	}
}

function createRacion() {
	var createRacion = document.querySelectorAll('[data-btn-create]');

	if (createRacion.length > 0) {

		for (var i = 0; i < createRacion.length; i++) {
			createRacion[i].addEventListener('click', function() {
				createIntakes(1);
				createIntakesModal();
				selectModalEatTime();
				addNewDay(document.getElementById('add-day-menu-personal-js'));
				hideModalBullet(document.querySelector('.modal--create-racion .field-radio__name--personal input:checked'));
				setRadioModalMenuPersonal(document.querySelector('.modal--create-racion .field-radio__name--personal input:checked'));
				hideModalsFoodItem(document.querySelector('.modal--create-racion .field-radio__name--personal input:checked'));
			});
		}
	}
}

function setRadioModalMenuPersonal(input) {
	var trueIntakes = input.dataset.trueIntakes;
	var modalInputs = document.querySelectorAll('.modal--menu-personal .field-radio__name--personal input');

	for (var i = 0; i < modalInputs.length; i++) {

		if (modalInputs[i].parentElement.classList.contains('field-radio__name--checked')) {
			modalInputs[i].parentElement.classList.remove('field-radio__name--checked');
		}

		var modalInputTrueIntakes = modalInputs[i].dataset.trueIntakes;

		if (trueIntakes === modalInputTrueIntakes) {
			modalInputs[i].checked = true;
			modalInputs[i].parentElement.classList.add('field-radio__name--checked');
		}
	}
}


function checkExcessKkal() {
	var racion = document.getElementById('racion-js');
	var normBlocks = document.querySelectorAll('.racion__intake-day-norm');

	for (var i = 0; i < normBlocks.length; i++) {
		var kkalValue = normBlocks[i].firstElementChild;

		if (+kkalValue.textContent >= 0) {

			if (normBlocks[i].classList.contains('danger')) {
				normBlocks[i].classList.remove('danger');
			}

			if (mediaUpLGRacion) {
				var alert = normBlocks[i].previousElementSibling;

				if (alert.classList.contains('visible')) {
					alert.classList.remove('visible');
				}
			}
		} else if (+kkalValue.textContent < 0) {

			if (!normBlocks[i].classList.contains('danger')) {
				normBlocks[i].classList.add('danger');
			}

			if (mediaUpLGRacion) {
				var alert = normBlocks[i].previousElementSibling;

				if (!alert.classList.contains('visible')) {
					alert.classList.add('visible');
				}
			}
		}
	}

	// if (mediaDownLG) {

	// }
}

function changeIntakes() {
	var modal = document.querySelector('.modal--menu-personal');

	if (modal) {
		var inputs = modal.querySelectorAll('.field-radio__name--personal input');

		for (var i = 0; i < inputs.length; i++) {

			inputs[i].addEventListener('change', function(event) {
				hideIntakes(event.target);
				hideModalsFoodItem(event.target);
				var racionSection = document.getElementById('racion-js');
				var trueIntakes = event.target.dataset.trueIntakes;
				var dataHiddenIntakes = event.target.dataset.hiddenIntakes;
				dataHiddenIntakes = dataHiddenIntakes.split(', ');


				var slides = racionSection.querySelectorAll('.swiper-slide');

				if (mediaDownLGRacion) {
					hideModalBullet(event.target);

					for (var i = 0; i < slides.length; i++) {
						var slide = slides[i];
						var slideIntake = slide.firstElementChild;
						var eatTimeSlideIntake = slideIntake.getAttribute('eat-time');

						for (var k = 0; k < dataHiddenIntakes.length; k++) {

							if (eatTimeSlideIntake === dataHiddenIntakes[k]) {

								var dishesItems = slide.querySelectorAll('.racion__dishes-item--not-empty');

								if (dishesItems.length > 0) {

									for (var j = 0; j < dishesItems.length; j++) {
										var idItem = dishesItems[j].dataset.id;

										var intakeIdItem = dishesItems[j].parentElement.parentElement;

										$.ajax({
											url: urlJson,
											dataType: 'json',
											data: {id:intakeIdItem},
											success: function(result) {
												var object = result[idItem];
												selectedItem.itemId = object.itemId;
												selectedItem.weight = object.weight;
												selectedItem.kkal = object.kkal;
												selectedItem.price = object.price;
												selectedItem.proteins = object.proteins;
												selectedItem.fat = object.fat;
												selectedItem.carbohydrates = object.carbohydrates;
												selectedItem.miniImg = object.miniImg;
												selectedItem.name = object.name;

												var numCurrentDay = intakeIdItem.getAttribute('current-day');

												var intakesCurrentDay = racionSection.querySelectorAll('.racion__intake[current-day=\"' + numCurrentDay + '\"]');
												var firstIntake = intakesCurrentDay[0];
												var lastIntake = intakesCurrentDay[intakesCurrentDay.length - 1];
												var dayNorm =  firstIntake.querySelector('.racion__intake-day-value');
												var totalCost = lastIntake.querySelector('.racion__intake-add-price-value');

												if (trueIntakes === '3') {
													calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'remove', numCurrentDay);
													calculateCostMenuPersonal(selectedItem.price, totalCost, 'remove');
												} else if (trueIntakes === '5') {
													calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', numCurrentDay);
													calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
												}
											}
										});
									}
								}
							}
						}
					}

					checkExcessKkal();

				} else if (mediaUpLGRacion) {
					hideIntakesItemDropdown(event.target);

					for (var i = 0; i < slides.length; i++) {
						var slide = slides[i];
						var dayNorm = slide.querySelector('.racion__intake-day-value');
						var totalCost = slide.querySelector('.racion__intake-add-price-value');
						var intakes = slide.querySelectorAll('.racion__intake');

						for (var j = 0; j < intakes.length; j++) {
							var eatTime = intakes[j].getAttribute('eat-time');

							for (var k = 0; k < dataHiddenIntakes.length; k++) {

								if (dataHiddenIntakes[k] === eatTime) {
									var intake = intakes[j];
									var dishesItems = intake.querySelectorAll('.racion__dishes-item--not-empty');

									if (dishesItems.length > 0) {

										for (var m = 0; m < dishesItems.length; m++) {
											var idItem = dishesItems[m].dataset.id;

											$.ajax({
												url: urlJson,
												dataType: 'json',
												data: {id:idItem},
												success: function(result) {
													var object = result[idItem];
													selectedItem.itemId = object.itemId;
													selectedItem.weight = object.weight;
													selectedItem.kkal = object.kkal;
													selectedItem.price = object.price;
													selectedItem.proteins = object.proteins;
													selectedItem.fat = object.fat;
													selectedItem.miniImg = object.miniImg;
													selectedItem.name = object.name;

													if (trueIntakes === '3') {
														calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'remove');
														calculateCostMenuPersonal(selectedItem.price, totalCost, 'remove');
													} else if (trueIntakes === '5') {
														calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add');
														calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
													}
												}
											});
										}
									}
								}
							}
						}

						checkExcessKkal();
					}
				}
				racionSwiper.update(true);
				// $.ajax({
				// 	url: urlJson,
				// 	dataType: 'json',
				// 	data: {id:idItem},
				// 	success: function(result) {
				// 		var slides = racionSection.querySelectorAll('.swiper-slide');

				// 		if (mediaDownLG) {
				// 			hideModalBullet(event.target);

				// 			for (var i = 0; i < slides.length; i++) {
				// 				var slide = slides[i];
				// 				var slideIntake = slide.firstElementChild;
				// 				var eatTimeSlideIntake = slideIntake.getAttribute('eat-time');

				// 				for (var k = 0; k < dataHiddenIntakes.length; k++) {

				// 					if (eatTimeSlideIntake === dataHiddenIntakes[k]) {

				// 						var dishesItems = slide.querySelectorAll('.racion__dishes-item--not-empty');

				// 						if (dishesItems.length > 0) {

				// 							for (var j = 0; j < dishesItems.length; j++) {
				// 								var idItem = dishesItems[j].dataset.id;

				// 								var intakeIdItem = dishesItems[j].parentElement.parentElement;

				// 								var object = result[idItem];
				// 								selectedItem.itemId = object.itemId;
				// 								selectedItem.weight = object.weight;
				// 								selectedItem.kkal = object.kkal;
				// 								selectedItem.price = object.price;
				// 								selectedItem.proteins = object.proteins;
				// 								selectedItem.fat = object.fat;
				// 								selectedItem.carbohydrates = object.carbohydrates;
				// 								selectedItem.miniImg = object.miniImg;

				// 								var numCurrentDay = intakeIdItem.getAttribute('current-day');

				// 								var intakesCurrentDay = racionSection.querySelectorAll('.racion__intake[current-day=\"' + numCurrentDay + '\"]');
				// 								var firstIntake = intakesCurrentDay[0];
				// 								var lastIntake = intakesCurrentDay[intakesCurrentDay.length - 1];
				// 								var dayNorm =  firstIntake.querySelector('.racion__intake-day-value');
				// 								var totalCost = lastIntake.querySelector('.racion__intake-add-price-value');

				// 								if (trueIntakes === '3') {
				// 									calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'remove', numCurrentDay);
				// 									calculateCostMenuPersonal(selectedItem.price, totalCost, 'remove');
				// 								} else if (trueIntakes === '5') {
				// 									calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', numCurrentDay);
				// 									calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
				// 								}
				// 							}
				// 						}
				// 					}
				// 				}
				// 			}

				// 			checkExcessKkal();
				// 		} else if (mediaUpLG) {
				// 			hideIntakesItemDropdown(event.target);

				// 			for (var i = 0; i < slides.length; i++) {
				// 				var slide = slides[i];
				// 				var dayNorm = slide.querySelector('.racion__intake-day-value');
				// 				var totalCost = slide.querySelector('.racion__intake-add-price-value');
				// 				var intakes = slide.querySelectorAll('.racion__intake');

				// 				for (var j = 0; j < intakes.length; j++) {
				// 					var eatTime = intakes[j].getAttribute('eat-time');

				// 					for (var k = 0; k < dataHiddenIntakes.length; k++) {

				// 						if (dataHiddenIntakes[k] === eatTime) {
				// 							var intake = intakes[j];
				// 							var dishesItems = intake.querySelectorAll('.racion__dishes-item--not-empty');

				// 							if (dishesItems.length > 0) {

				// 								for (var m = 0; m < dishesItems.length; m++) {
				// 									var idItem = dishesItems[m].dataset.id;

				// 									var object = result[idItem];
				// 									selectedItem.itemId = object.itemId;
				// 									selectedItem.weight = object.weight;
				// 									selectedItem.kkal = object.kkal;
				// 									selectedItem.price = object.price;
				// 									selectedItem.proteins = object.proteins;
				// 									selectedItem.fat = object.fat;
				// 									selectedItem.miniImg = object.miniImg;

				// 									if (trueIntakes === '3') {
				// 										calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'remove');
				// 										calculateCostMenuPersonal(selectedItem.price, totalCost, 'remove');
				// 									} else if (trueIntakes === '5') {
				// 										calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add');
				// 										calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
				// 									}
				// 								}
				// 							}
				// 						}
				// 					}
				// 				}

				// 				checkExcessKkal();
				// 			}
				// 		}
				// 		racionSwiper.update(true);
				// 	}
				// });
			});
		}
	}
}

function createIntakesModalDropdown(numIntakes, namesIntakes) {

	if (mediaUpLGRacion) {
		var modal = $('.modal--menu-categories-block');
		var btn = $('#modal__add-racion-js');

		var dropdown = document.createElement('div');
		dropdown.className = 'dropdown-menu';

		for (var j = 0; j < numIntakes; j++) {
			var dropdownBtn = document.createElement('button');
			dropdownBtn.className = 'dropdown-item';
			dropdownBtn.type = 'button';

			var dropdownBtnSpan = document.createElement('span');
			dropdownBtnSpan.append(namesIntakes[j]);

			dropdownBtn.append(dropdownBtnSpan);
			dropdownBtn.setAttribute('eat-time', namesIntakes[j]);
			selectModalBulletEatTime(dropdownBtn);
			dropdown.append(dropdownBtn);
		}

		btn.after(dropdown);
	}
}

function selectModalBulletEatTime(btn) {

	$(btn).click(function(event) {
		var modal = $('.modal--menu-personal-block');
		var currentDay = $('.racion .swiper-slide-active');
		var numCurrentDay = currentDay[0].getAttribute('current-day');

		var btnEatTime = $(this).attr('eat-time');
		var intakeCurrentDay = currentDay.find('.racion__intake[eat-time=\"' + btnEatTime + '\"]');

		var dishes = intakeCurrentDay.find('.racion__dishes');
		var dishesItem = intakeCurrentDay.find('.racion__dishes-item:not(".racion__dishes-item--not-empty")');
		var imgDishesItem = dishesItem.find('.racion__dishes-image');
		var nameplateDishesItem = dishesItem.find('.racion__dishes-nameplate');
		var priceDishesItem = dishesItem.find('.racion__dishes-price');

		dishesItem.addClass('racion__dishes-item--not-empty');

		dishesItem[0].dataset.id = selectedItem.itemId;
		imgDishesItem.prop('src', selectedItem.miniImg);
		nameplateDishesItem.append(selectedItem.kkal);
		priceDishesItem.append(selectedItem.price + ' р.');
		modal.fadeOut(400);
		doubleDishesItem(dishes[0]);

		var dayNorm = currentDay[0].querySelector('.racion__intake-day-value');
		var totalCost = currentDay[0].querySelector('.racion__intake-add-price-value');
		calculateKkalMenuPersonal(selectedItem.kkal, dayNorm, 'add', numCurrentDay);
		calculateCostMenuPersonal(selectedItem.price, totalCost, 'add');
		addDishesToModal(numCurrentDay, btnEatTime, selectedItem);
		calculateRacionPopupBgHeight();
	});
}

function checkMenuPersonalPage() {
	var modal = document.querySelector('.modal--create-racion');

	if (modal) {
		var body = document.querySelector('body');

		body.classList.add('no-scroll');

		calculateModalIndents('.modal--create-racion');
	}
}

function getLastNavItem() {
		if ($('.main-nav__item--double').length > 0) {
		var firstDoubleLink = document.querySelector('.main-nav__item--double');
		var lastNavItem = firstDoubleLink.previousElementSibling;

		if (lastNavItem) {
			lastNavItem.classList.add('main-nav__item--last-main');
		}
	}
}
